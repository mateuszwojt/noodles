from nodes.node import Node


class ImageNode(Node):
    _image = None
    
    def __init__(self):
        super(ImageNode, self).__init__()

    def format(self):
        return self._format

    def full_format(self):
        return self._full_format

    def set_format(self, d):
        pass

    def format_changed(self):
        pass

    def aspect_ratio(self):
        return self._aspect

    def set_aspect_ratio(self, a):
        pass

    def add_output_plug(self):
        pass


