

class NodeMetaclass:
    id_ = None
    major_version = 1
    minor_version = 0
    ui_visible = None
    menu = None
    submenu = None
    menu_item = None
    hotkey = None
    help = None
    _first_time = None

    def __init__(self):
        self._ui_visible = True
        self._first_time = True

    def cleanup(self):
        raise NotImplementedError

    def create(self):
        raise NotImplementedError

    def create_gui(self):
        raise NotImplementedError
