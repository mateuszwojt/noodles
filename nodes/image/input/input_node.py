from nodes.node_factory import NodeFactory
from nodes.node_metaclass import NodeMetaclass
from nodes.image_node import ImageNode


class InputNode(ImageNode):
    # _readers = []
    # _clips = []

    # unique node identifier.
    __identifier__ = 'nodes.input.input_node'

    NODE_NAME = 'Input'

    def __init__(self):
        super(InputNode, self).__init__()

        self.add_input('in A')
        self.add_input('in B')

        self.add_output('out A')
        self.add_output('out B')

    def create_image_input_node(self):
        pass

    def create_gui_image_input_node(self):
        pass

    def metaclass(self):
        return self.image_input_node_metaclass()

    @classmethod
    def image_input_node_metaclass(cls):
        info = NodeMetaclass()
        info.id = 'image.builtin.image_input'
        info.major_version = 1
        info.minor_version = 0
        info.menu = 'Image'
        info.submenu = 'Input'
        info.menu_item = 'Image...'
        info.ui_visible = True
        info.create = cls.create_image_input_node
        info.create_gui = cls.create_gui_image_input_node

        return info

    def registered(self):
        return NodeFactory().register_node(self.image_input_node_metaclass())
