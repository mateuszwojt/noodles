import importlib.util
import inspect
import fnmatch
import logging
import os.path
import sys
from pathlib import Path

from typing import Tuple, List

from nodegraph.errors import NodeRegistrationError

from nodes.node import Node
from nodes.node_metaclass import NodeMetaclass


def _import_module_from_path(filepath: Path, module_name: str = None) -> str:
    module_name = module_name or f"{filepath.stem}"
    spec = importlib.util.spec_from_file_location(
        module_name,
        filepath)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module_name


def _get_node_class_metaclass(module_name: str):
    cls_members = inspect.getmembers(sys.modules[module_name], inspect.isclass)
    for _, cls in cls_members:
        if cls.__module__ == module_name and issubclass(cls, Node):
            return cls().metaclass()


def _get_node_class(module_name: str):
    cls_members = inspect.getmembers(sys.modules[module_name], inspect.isclass)
    for _, cls in cls_members:
        if cls.__module__ == module_name and issubclass(cls, Node):
            return cls


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class NodeFactory(metaclass=Singleton):
    """This class is a singleton to make sure we keep a single register of all nodes."""
    _names = {}
    _aliases = {}
    _nodes = {}

    def __init__(self):
        self._register_nodes()

    def _register_nodes(self):
        """
        Traverse the `./image` directory and auto-discover new nodes by
        importing the modules from the *.py files.

        Registers each module as a new node class.
        """
        current_dir = os.path.dirname(os.path.realpath(__file__))
        for root, _, filenames in os.walk(os.path.join(current_dir, 'image')):
            for filename in fnmatch.filter(filenames, '*.py'):
                # skip __init__.py files
                if not filename.startswith('__init__'):
                    filepath = Path(os.path.join(root, filename))
                    module_name = _import_module_from_path(filepath)
                    metaclass = _get_node_class(module_name)
                    self.register_node(metaclass)

    # def register_node(self, node_metaclass: NodeMetaclass):
    #     # TODO: check for duplicates
    #     self._metaclasses.append(node_metaclass)

    def sort_by_menu_item(self):
        # TODO:
        pass

    def registered_nodes(self) -> List[NodeMetaclass]:
        return self._nodes

    def create_by_id_with_version(self, id_, version: Tuple[int, int]):
        best_minor = -1
        best = None

        for node in self._nodes:
            if node.id_ == id_ and node.major_version == version[0] and node.minor_version > best_minor:
                best = node
                best_minor = node.minor_version

        if best_minor < version[1]:
            return Node()

        if best in self._metaclasses:
            if best.first_time:
                best.first_time = False

                if best.init:
                    best.init()

            return best.create()

    def is_latest_version(self, id_):
        # TODO:
        return True

    @property
    def names(self):
        """
        Return all currently registered node type identifiers.

        Returns:
            dict: key=<node name, value=node_type
        """
        return self._names

    @property
    def aliases(self):
        """
        Return aliases assigned to the node types.

        Returns:
            dict: key=alias, value=node type
        """
        return self._aliases

    @property
    def nodes(self):
        """
        Return all registered nodes.

        Returns:
            dict: key=node identifier, value=node class
        """
        return self._nodes

    def create_node_instance(self, node_type=None):
        """
        create node object by the node type identifier or alias.

        Args:
            node_type (str): node type or optional alias name.

        Returns:
            nodegraph.NodeObject: new node object.
        """
        if node_type in self.aliases:
            node_type = self.aliases[node_type]

        _NodeClass = self._nodes.get(node_type)
        if not _NodeClass:
            print('can\'t find node type {}'.format(node_type))
        return _NodeClass()

    def register_node(self, node, alias=None):
        """
        register the node.

        Args:
            node (nodegraph.NodeObject): node object.
            alias (str): custom alias for the node identifier (optional).
        """
        if node is None:
            return

        name = node.NODE_NAME
        node_type = node.type_

        if self._nodes.get(node_type):
            logging.warning(
                'node type "{}" already registered to "{}"! '
                'Please specify a new plugin class name or __identifier__.'
                .format(node_type, self._nodes[node_type])
            )
            return
        self._nodes[node_type] = node

        if self._names.get(name):
            self._names[name].append(node_type)
        else:
            self._names[name] = [node_type]

        if alias:
            if self._aliases.get(alias):
                raise NodeRegistrationError(
                    'Alias: "{}" already registered to "{}"'
                    .format(alias, self._aliases.get(alias))
                )
            self._aliases[alias] = node_type

    def clear_registered_nodes(self):
        """
        clear out registered nodes, to prevent conflicts on reset.
        """
        self._nodes.clear()
        self._names.clear()
        self._aliases.clear()
