from abc import abstractmethod

from PySide6 import QtCore

from nodegraph import BaseNode


class Node(BaseNode):
    _inputs = []
    _outputs = []

    # signals
    changed = QtCore.Signal()

    def __init__(self):
        super(Node, self).__init__()

    def cloned(self):
        raise NotImplementedError

    def num_inputs(self):
        return len(self._inputs)

    # @abstractmethod
    def add_input_plug(self, id_: str, optional: bool, color, tooltip: str):
        pass

    def has_output_plug(self) -> bool:
        return len(self._outputs) == 0

    def num_outputs(self) -> int:
        return len(self._outputs)

    @abstractmethod
    def metaclass(self):
        pass





