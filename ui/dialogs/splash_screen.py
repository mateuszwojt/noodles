from PySide6 import QtWidgets, QtGui, QtCore


class SplashScreen:
    _splash = None

    def __init__(self):
        pixmap = QtGui.QPixmap(':/splash.png')
        self._splash = QtWidgets.QSplashScreen(pixmap, QtCore.Qt.WindowStaysOnTopHint)
        self._splash.setEnabled(False)

    def show(self):
        self._splash.show()
        QtWidgets.QApplication.instance().processEvents()

    def show_message(self, msg):
        self._splash.showMessage(msg, QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter, QtCore.Qt.black)
        QtWidgets.QApplication.instance().processEvents()

    def finish(self, widget):
        self._splash.finish(widget)
