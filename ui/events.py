from ui.viewer.viewer_strategy import ViewerStrategy


class PaintEvent:
    view = ViewerStrategy()
    aspect_ratio = 1.0
    pixel_scale = 1.0
    subsample = 1
