from PySide6 import QtWidgets, QtCore, QtGui

from nodegraph.widgets.viewer import NodeViewer

from ui.compview.composition_view_toolbar import CompositionViewToolbar
from ui.compview.composition_view_layout import CompositionViewLayout


class CompositionView(QtWidgets.QWidget):
    _font = None
    _scroll_mode = None
    _zoom_mode = None
    _first_resize = None
    _connect_mode = None
    _box_pick_mode = None
    _toolbar = None
    _viewport = None
    _layout = CompositionViewLayout()
    _last_x = None
    _last_y = None
    _drag_handler = None
    _release_handler = None

    def __init__(self, parent=None):
        super(CompositionView, self).__init__(parent)

        self.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.setMouseTracking(True)
        self._font = QtGui.QFont('Helvetica', 10)
        self._scroll_mode = False
        self._first_resize = True
        self._connect_mode = False
        self._box_pick_mode = False

        self._viewport = NodeViewer(undo_stack=QtGui.QUndoStack(self))

    def create_toolbar(self):
        self._toolbar = CompositionViewToolbar(self)
        return self._toolbar

    def viewport(self):
        return self._viewport

    def screen_to_world(self, pos):
        pass

    def world_to_screen(self, pos):
        pass

    def place_node(self, node):
        pass

    def place_node_near_node(self, node, other):
        pass

    # def event(self, event):
    #     return True

    # def keyPressEvent(self, event: QtGui.QKeyEvent):
    #     event_key = event.key()
    #
    #     if event_key in [QtCore.Qt.Key_Delete, QtCore.Qt.Key_Backspace]:
    #         self.delete_selected_nodes()
    #     elif event_key == QtCore.Qt.Key_Home:
    #         self.viewport().reset()
    #         self._layout.set_world(self.viewport().world())
    #         self.update()
    #         event.accept()
    #     elif event_key == QtCore.Qt.Key_Comma:
    #         # TODO: zoom in
    #         pass
    #     elif event_key == QtCore.Qt.Key_Period:
    #         # TODO: zoom out
    #         pass
    #     elif event_key == QtCore.Qt.Key_C:
    #         # TODO: center selected
    #         pass
    #     elif event_key == QtCore.Qt.Key_F:
    #         # TODO: frame selected
    #         pass
    #     else:
    #         event.ignore()
    #
    # def keyReleaseEvent(self, event: QtGui.QKeyEvent):
    #     event_key = event.key()
    #     # TODO: accept keys from keyPressEvent method, ignore everything else
    #
    # def mouseDoubleClickEvent(self, event: QtGui.QMouseEvent):
    #     wpos = self.screen_to_world(event.position())
    #     self._layout.set_interest_point(wpos)
    #
    #     # TODO: last pick with Control
    #
    #     event.accept()
    #
    # def mousePressEvent(self, event: QtGui.QMouseEvent):
    #     event.accept()
    #
    #     # TODO: implement node picking
    #
    # def mouseMoveEvent(self, event: QtGui.QMouseEvent):
    #     if not event.buttons():
    #         return
    #
    #     if event.x() != self._last_x or event.y() != self._last_y:
    #         if self._drag_handler:
    #             self._drag_handler(event)
    #         self._last_x = event.x()
    #         self._last_y = event.y()
    #         event.accept()
    #
    # def mouseReleaseEvent(self, event: QtGui.QMouseEvent):
    #     wpos = self.screen_to_world(event.position())
    #     self._layout.set_interest_point(wpos)
    #
    #     if self._release_handler:
    #         self._release_handler(event)
    #
    #     self._scroll_mode = False
    #     self._zoom_mode = False
    #     event.accept()
    #
    # def wheelEvent(self, event: QtGui.QWheelEvent):
    #     wpos = self.screen_to_world(event.position())
    #     self._layout.set_interest_point(wpos)
    #
    #     if event.pixelDelta().y() > 0:
    #         self.viewport().zoom(wpos, 1.10)
    #     else:
    #         self.viewport().zoom(wpos, 1 / 1.10)
    #
    #     self.update()
    #     event.accept()





