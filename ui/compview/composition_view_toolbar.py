from PySide6 import QtWidgets

from utils.constants import QWIDGETSIZE_MAX


class CompositionViewToolbar(QtWidgets.QWidget):
    _comp_view = None
    _toolbar_height = 30

    def __init__(self, comp_view, parent=None):
        super(CompositionViewToolbar, self).__init__(parent)

        self._comp_view = comp_view

        self.setMinimumSize(0, self._toolbar_height)
        self.setMaximumSize(QWIDGETSIZE_MAX, self._toolbar_height)
        self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)

    def toolbar_height(self):
        return self._toolbar_height
