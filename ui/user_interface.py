import logging

from PySide6 import QtWidgets, QtCore, QtGui

from nodegraph.custom_widgets.properties_bin import PropertiesBinWidget

import render.context
import render.image_node_renderer
from nodes.node import Node
from serialization.yaml_iarchive import YamlIArchive
from serialization.yaml_oarchive import YamlOArchive
from ui.main_window import MainWindow
from ui.inspector.inspector import Inspector
from ui.viewer.viewer import Viewer


class UserInterface(QtCore.QObject):
    _active = None
    _context = None
    _rendering = False
    _cancelled = False
    _interacting = False
    _event_filter_installed = False

    # widgets
    _viewer = None
    _inspector = None
    _anim_editor = None
    _window = None

    def __init__(self, parent=None):
        super(UserInterface, self).__init__(parent)

        # TODO: init image types list

    def init(self):
        self.init_ui_style()

        self.create_new_document()

        self._viewer = Viewer()
        self._inspector = PropertiesBinWidget()
        # self._anim_editor = AnimEditor()
        self._window = MainWindow()
        self.restore_window_state()

    def init_ui_style(self):
        pass

    def save_window_state(self):
        pass

    def restore_window_state(self):
        pass

    def show(self):
        self._window.show()

    def run(self, path):
        self.open_document(path)

    def quit(self):
        self.parent().set_quitting(True)
        self.save_window_state()
        QtWidgets.QApplication.instance().quit()

    def create_new_document(self):
        self.set_active_node(None)
        self.set_context_node(None)

        QtWidgets.QApplication.instance().create_new_document()

        self.update()

    def open_document(self, path: str):
        archive = YamlIArchive(path)
        if not archive.read_composition_header():
            logging.warning('Cannot read file header %s' % path)
            self.create_new_document()

        QtWidgets.QApplication.instance().document().set_file(path)
        QtWidgets.QApplication.instance().document().load(archive)
        self.main_window().update_recent_files_menu(path)
    def save_document(self):
        archive = QtWidgets.QApplication.instance().document().save()
        archive.write_composition_header()
        # self.write_ui_state()

        archive.write_to_file(QtWidgets.QApplication.instance().document().file())
        QtWidgets.QApplication.instance().document().set_dirty(False)

    def active_node(self) -> Node:
        return self._active

    def set_active_node(self, node: Node):
        self._active = node

    def context_node(self) -> Node:
        return self._context

    def set_context_node(self, node: Node):
        self._context = node

    def node_added(self, node: Node):
        self.viewer().node_added(node)
        QtWidgets.QApplication.instance().document().composition()._added.emit(node)

    def node_released(self, node: Node):
        if node == self._active:
            self.set_active_node(None)
            self.update()

        if node == self._context:
            self.set_context_node(node)
            self.update()

        self.viewer().node_released(node)
        QtWidgets.QApplication.instance().document().composition()._released.emit(node)

    def update(self):
        if not QtWidgets.QApplication.instance().quitting():
            if self._window:
                self._window.update()
            self.update_anim_editors()

    def begin_interaction(self):
        pass

    def end_interaction(self):
        pass

    def start_frame(self):
        pass

    def end_frame(self):
        pass

    def frame(self):
        pass

    def set_start_frame(self, frame):
        composition = QtWidgets.QApplication.instance().document().composition()
        composition.set_start_frame(frame)
        self.main_window().time_slider().update(composition.start_frame(),
                                                composition.frame(),
                                                composition.end_frame())

    def set_end_frame(self, frame):
        pass

    def set_frame(self, frame):
        pass

    def update_anim_editors(self):
        if self._anim_editor:
            self.anim_editor().update()

    def fatal_error(self, msg):
        QtWidgets.QMessageBox.critical(None, 'Fatal Error', msg)

    def error(self, msg):
        QtWidgets.QMessageBox.warning(self.main_window(), 'Fatal Error', msg)

    def inform(self, msg):
        QtWidgets.QMessageBox.information(self.main_window(), 'Fatal Error', msg)

    def question(self, what, default_answer):
        pass

    def main_window(self):
        return self._window

    def viewer(self):
        return self._viewer

    def inspector(self) -> PropertiesBinWidget:
        return self._inspector

    def anim_editor(self):
        return self._anim_editor

    def start_long_process(self):
        self._cancelled = False
        QtWidgets.QApplication.instance().installEventFilter()
        self._event_filter_installed = True

    def process_events(self):
        QtWidgets.QApplication.instance().processEvents()

    def process_cancelled(self):
        return self._cancelled

    def end_long_process(self):
        QtWidgets.QApplication.instance().removeEventFilter()
        self._event_filter_installed = False

    def eventFilter(self, watched, event: QtCore.QEvent):
        event_type = event.type()

        if event_type == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Escape:
            self._cancelled = True
            return True

        if event_type == QtCore.QEvent.MouseMove:
            return event.buttons() == QtCore.Qt.NoButton

        return False

    def render_image(self, context: render.context.Context, renderer: render.image_node_renderer.ImageNodeRenderer):
        self._cancelled = False
        context.mode = render.context.RenderMode.INTERFACE_RENDER
        context.cancel = self.process_cancelled
        renderer.set_context(context)
        self._rendering = True

        future = QtWidgets.QApplication.instance().render_thread().render_image(renderer)
        # if task.is_finished():
        #     self._rendering = False
        #     return task
        #
        # task.wait()
        # self._rendering = False
        # return task

    def viewer_toolbar_height(self):
        return self.viewer().toolbar_height()

    def get_fixed_width_code_font(self):
        font = QtGui.QFont()
        font.setFamily('Courier')
        font.setFixedPitch(True)
        font.setPointSize(11)
        return font
