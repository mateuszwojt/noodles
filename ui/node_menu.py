from typing import Union

from PySide6 import QtWidgets, QtGui


class NodeMenu:
    _name = None
    _menu = None
    _submenus = []

    def __init__(self, name: str):
        self._menu = QtWidgets.QMenu(name)
        self._name = name

    def name(self) -> str:
        return self._name

    def menu(self):
        return self._menu

    def submenus(self):
        return self._submenus

    def add_submenu(self, name: str):
        self._submenus.append(self._menu.addMenu(name))

    def add_action(self, submenu: str, action: QtGui.QAction):
        menu = self.find_submenu(submenu)

        if menu == len(self._submenus):
            self._submenus.append(self._menu.addMenu(submenu))
            menu = self.find_submenu(submenu)

        menu.addAction(action)

    def find_submenu(self, name: str) -> Union[QtWidgets.QMenu, int]:
        for m in self._submenus:
            if m.title() == name:
                return m
        return len(self._submenus)
