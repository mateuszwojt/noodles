import logging

from PySide6 import QtCore, QtWidgets, QtGui

from utils import clamp
from ui.widgets.spinbox import Spinbox


class DoubleSpinbox(Spinbox):
    # signals
    valueChanged = QtCore.Signal(float)
    spinBoxPressed = QtCore.Signal()
    spinBoxDragged = QtCore.Signal(float)
    spinBoxReleased = QtCore.Signal()

    def __init__(self, parent=None):
        super(DoubleSpinbox, self).__init__(parent)

        self._value = 0
        self._previous_value = 0
        self.setText('0')
        # self.editingFinished.connect(self.textChanged)

    def setSuffix(self, suffix):
        # TODO: implement
        raise NotImplementedError

    def value(self):
        return self._value

    def setValue(self, value):
        if self.decimals():
            value = int(value)
        value = clamp(value, self.minimum(), self.maximum())
        self.setLineEditContents(value)
        value = clamp(value, self.minimum(), self.maximum())

        if value == self._value:
            return False

        self._previous_value = value
        self._value = value
        return True

    def restorePreviousValue(self):
        self.setValue(self._previous_value)

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        event_key = event.key()
        if event_key == QtCore.Qt.Key_Up or event_key == QtCore.Qt.Key_Down:
            if not event.isAutoRepeat():
                if self.trackMouse():
                    self.spinBoxPressed.emit()

            self.stepBy(1 if event_key == QtCore.Qt.Key_Up else -1)
            return
        else:
            super(DoubleSpinbox, self).keyPressEvent(event)

    def keyReleaseEvent(self, event: QtGui.QKeyEvent):
        event_key = event.key()
        if event_key == QtCore.Qt.Key_Up or event_key == QtCore.Qt.Key_Down:
            if self.trackMouse():
                self.spinBoxReleased.emit()
            else:
                self.valueChanged.emit(self._value)
        else:
            super(DoubleSpinbox, self).keyReleaseEvent(event)

    def mousePressEvent(self, event: QtGui.QMouseEvent):
        self._drag = False
        self._dragged = False
        self._first_drag = True

        if event.button() == QtCore.Qt.LeftButton and event.modifiers() == QtCore.Qt.AltModifier:
            self._drag = True
            self._push_x = event.x()
            self._last_x = self._push_x
        else:
            super(DoubleSpinbox, self).mousePressEvent(event)

    def mouseMoveEvent(self, event: QtGui.QMouseEvent):
        if self._drag:
            if event.x() != self._last_x:
                if self._first_drag:
                    if abs(event.x() - self._push_x) >= 3:
                        if self.trackMouse():
                            self.spinBoxPressed.emit()

                        self._first_drag = False
                        self._dragged = True
                    else:
                        return

                self.stepBy(event.x() - self._last_x)

            self._last_x = event.x()
        else:
            super(DoubleSpinbox, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event: QtGui.QMouseEvent):
        if self._drag:
            if self._dragged:
                if self.trackMouse():
                    self.spinBoxReleased.emit()
                else:
                    self.valueChanged.emit(self._value)

            self._dragged = False
            self._drag = False
            self._first_drag = True
        else:
            super(DoubleSpinbox, self).mouseReleaseEvent(event)

    def textChanged(self, *args, **kwargs):
        if self.isModified():
            self.setModified(False)
            s = self.text()

            # TODO: add formula calculation
            if self.setValue(s):
                self.valueChanged.emit(self.value())

            # TODO: restore previous value if calculation failed

    def stepBy(self, steps):
        if self.setValue(self.value() + steps * self.singleStep()):
            if self.trackMouse():
                self.spinBoxDragged.emit(self.value())
