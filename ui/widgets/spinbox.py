import sys

from PySide6 import QtWidgets, QtGui, QtCore


class Spinbox(QtWidgets.QLineEdit):
    _push_x = None
    _last_x = None

    _thousand_sep = 0
    _decimal_sep = 0

    def __init__(self, parent=None):
        super(Spinbox, self).__init__(parent)

        self._decimals = 5
        self._min = -sys.maxsize
        self._max = -self._min
        self._step = 1
        self._track_mouse = True

        self._drag = False
        self._dragged = False
        self._first_drag = False

        self._copy = QtGui.QAction('copy', self)
        self._paste = QtGui.QAction('paste', self)

    def sizeHint(self):
        size = QtWidgets.QLineEdit.sizeHint(self)
        fm = QtGui.QFontMetrics(self.font())
        size.setWidth(fm.averageCharWidth() * 8)
        return size

    def setLineEditContents(self, value):
        str_ = self.locale().toString(float(value), 'f', self.decimals())
        str_.replace(self.thousandSeparator(), '')

        index_ = str_.find(self.decimalSeparator())
        if index_ != -1:
            for i in reversed(range(len(str_) - 1)):
                # replace zeroes with whitespace
                if str_[i] == '0':
                    str_ = str_[:i] + ' ' + str_[i+1:]
                else:
                    break
            # remove leading and trailing whitespace and truncate string
            str_.strip()
            if len(str_) - 1 == self.decimalSeparator():
                str_ = str_[0:len(str_) - 1]

        self.setText(str_)
        self.setModified(False)

    def thousandSeparator(self):
        if self._thousand_sep == 0:
            str_ = self.locale().toString(1100.0, 'f')
            self._thousand_sep = str_[1]
        return self._thousand_sep

    def decimalSeparator(self):
        if self._decimal_sep == 0:
            str_ = self.locale().toString(0.1, 'f')
            self._decimal_sep = str_[1]
        return self._decimal_sep

    def decimals(self):
        return self._decimals

    def setDecimals(self, value: int):
        if value >= 0:
            self._decimals = value

    def minimum(self):
        return self._min

    def setMinimum(self, value: float):
        self._min = value

    def maximum(self):
        return self._max

    def setMaximum(self, value: float):
        self._max = value

    def setRange(self, min_: float, max_: float):
        if min_ <= max_:
            self._min = min_
            self._max = max_

    def singleStep(self):
        return self._step

    def setSingleStep(self, value: float):
        self._step = value

    def trackMouse(self):
        return self._track_mouse

    def setTrackMouse(self, value: bool):
        self._track_mouse = value
