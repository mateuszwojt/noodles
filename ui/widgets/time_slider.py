import math
import numpy

from PySide6 import QtWidgets, QtCore, QtGui

from utils import clamp
from ui.widgets.double_spinbox import DoubleSpinbox


class TimeScale(QtWidgets.QWidget):
    _min_value = 1
    _max_value = 100
    _value = 1
    _drag = False
    _last_x = 0

    # signals
    valueChanged = QtCore.Signal(float)

    def __init__(self, parent=None):
        super(TimeScale, self).__init__(parent)

    def setRange(self, min_, max_):
        self._min_value = min_
        self._max_value = max_
        self.update()

    def setMinimum(self, value):
        self._min_value = value
        self.update()

    def setMaximum(self, value):
        self._max_value = value
        self.update()

    def setValue(self, value):
        new_val = clamp(value, self._min_value, self._max_value)

        if self._value != new_val:
            self._value = new_val
            self.valueChanged.emit(value)
            self.update()

    def round_halfup(self, x):
        result = math.floor(math.fabs(x) + 0.5)
        return -result if x < 0.0 else result

    def frame_from_mouse_pos(self, x):
        f = x / self.width()
        f = f * (self._max_value - self._min_value) + self._min_value

        return clamp(self.round_halfup(f), self._min_value, self._max_value)

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self._drag = True
            self._last_x = event.x()
            self.setValue(self.frame_from_mouse_pos(self._last_x))
        event.accept()

    def mouseMoveEvent(self, event):
        if self._drag:
            if self._last_x != event.x():
                self.setValue(self.frame_from_mouse_pos(event.x()))
            self._last_x = event.x()
        event.accept()

    def mouseReleaseEvent(self, event):
        event.accept()

    def paintEvent(self, event):
        painter = QtGui.QPainter()
        painter.begin(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)

        pen = QtGui.QPen()
        pen.setColor(QtGui.QColor(0, 0, 0))
        pen.setWidth(1)
        painter.setPen(pen)

        painter.drawLine(0, 7, self.width(), 7)

        spacing = 50
        nticks = math.floor(self.width() / spacing)
        coords = numpy.linspace(0 + 0.5 * self._min_value,
                                self.width() - 0.5 * self._min_value,
                                nticks, endpoint=True)

        for sx in numpy.round(coords):
            painter.drawLine(QtCore.QPointF(sx, 2), QtCore.QPointF(sx, 12))
            # FIXME: find way to draw frame numbers in correct place
            # painter.drawText(QtCore.QPoint(sx, self.height()), str(int(x)))

        # draw red frame marker
        pen.setColor(QtGui.QColor(255, 0, 0))
        pen.setWidth(3)
        painter.setPen(pen)

        x = (self._value - self._min_value) / (self._max_value - self._min_value) * self.width()
        painter.drawLine(QtCore.QPointF(x, 0), QtCore.QPointF(x, self.height()))

        painter.end()
        event.accept()


class TimeSlider(QtWidgets.QWidget):
    # signals
    start_frame_changed = QtCore.Signal(int)
    end_frame_changed = QtCore.Signal(int)
    time_changed = QtCore.Signal(int)

    def __init__(self, parent=None):
        super(TimeSlider, self).__init__(parent)

        self._start = DoubleSpinbox()
        self._start.setTrackMouse(False)
        self._start.setRange(-32768, 32768)
        self._start.setValue(1)
        self._start.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        self._end = DoubleSpinbox()
        self._end.setTrackMouse(False)
        self._end.setRange(-32768, 32768)
        self._end.setValue(100)
        self._end.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        self._current = DoubleSpinbox()
        self._current.setTrackMouse(False)
        self._current.setRange(1, 100)
        self._current.setValue(1)
        self._current.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

        self._scale = TimeScale()
        self._scale.setRange(1, 100)
        self._scale.setValue(1)

        self._start.valueChanged.connect(self.set_start_frame)
        self._end.valueChanged.connect(self.set_end_frame)
        self._current.valueChanged.connect(self.set_frame)
        self._scale.valueChanged.connect(self.set_frame)

        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(self._start)
        layout.addWidget(self._end)
        layout.addWidget(self._current)
        layout.addWidget(self._scale)
        self.setLayout(layout)

        self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)

    def update(self, start, frame, end):
        self.block_all_signals(True)

        self._start.setValue(start)
        self._end.setValue(end)

        self._current.setMinimum(start)
        self._current.setMaximum(end)
        self._current.setValue(frame)

        self._scale.setMinimum(start)
        self._scale.setMaximum(end)
        self._scale.setValue(frame)

        self.block_all_signals(False)

    def set_start_frame(self, value):
        self.block_all_signals(True)

        cur_frame = self._current.value()
        new_start = min(value, self._end.value())
        self._start.setValue(new_start)

        self._current.setMinimum(self._start.value())
        self._scale.setMinimum(self._start.value())

        self.block_all_signals(False)

        self.start_frame_changed.emit(self._start.value())
        self.adjust_frame(cur_frame)

    def set_end_frame(self, value):
        self.block_all_signals(True)

        cur_frame = self._current.value()
        new_end = min(value, self._start.value())
        self._end.setValue(new_end)

        self._current.setMinimum(self._end.value())
        self._scale.setMinimum(self._end.value())

        self.block_all_signals(False)

        self.end_frame_changed.emit(self._end.value())
        self.adjust_frame(cur_frame)

    def set_frame(self, value):
        self.block_all_signals(True)
        self._scale.setValue(value)
        self._current.setValue(value)
        self.block_all_signals(False)
        self.time_changed.emit(value)

    def block_all_signals(self, value: bool):
        self._start.blockSignals(value)
        self._end.blockSignals(value)
        self._current.blockSignals(value)
        self._scale.blockSignals(value)

    def adjust_frame(self, frame: int):
        new_value = clamp(frame, self._start.value(), self._end.value())

        if new_value != frame:
            self.block_all_signals(True)
            self._current.setValue(new_value)
            self._scale.setValue(new_value)
            self.block_all_signals(False)
            self.time_changed.emit(new_value)
