from PySide6 import QtCore, QtWidgets


class ContainerWidget(QtWidgets.QStackedWidget):
    _contents = None

    def __init__(self, parent=None):
        super(ContainerWidget, self).__init__(parent)

    def set_contents(self, widget: QtWidgets.QWidget):
        self.clear_contents()

        self.addWidget(widget)
        self.setCurrentIndex(0)
        self._contents = widget

    def clear_contents(self):
        if not self._contents:
            return

        self.removeWidget(self._contents)
        self._contents = None
