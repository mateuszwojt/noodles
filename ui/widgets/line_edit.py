from PySide6 import QtCore, QtWidgets


class LineEdit(QtWidgets.QLineEdit):
    def __init__(self, text, parent=None):
        super(LineEdit, self).__init__(text, parent)

    def contextMenuEvent(self, event):
        event.ignore()
