from PySide6 import QtCore
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *


class ViewerStrategy(QtCore.QObject):
    _active = False

    def __init__(self, parent=None):
        super(ViewerStrategy, self).__init__(parent)

    def init(self):
        pass

    def is_active(self) -> bool:
        return self._active

    def set_active(self, value: bool):
        self._active = value

    def begin_active_view(self):
        self.set_active(True)
        self.do_begin_active_view()

    def end_active_view(self):
        self.do_end_active_view()
        self.set_active(False)

    def do_begin_active_view(self):
        raise NotImplementedError

    def do_end_active_view(self):
        raise NotImplementedError

    def update(self):
        if self.parent():
            self.parent().update()

    def paint(self):
        glClear(GL_COLOR_BUFFER_BIT)

    def resize(self, w, h):
        pass

    def enter_event(self, event):
        event.accept()

    def leave_event(self, event):
        event.accept()

    def key_press_event(self, event):
        event.ignore()

    def key_release_event(self, event):
        event.ignore()

    def mouse_move_event(self, event):
        event.ignore()

    def mouse_press_event(self, event):
        event.ignore()

    def mouse_release_event(self, event):
        event.ignore()

    def wheel_event(self, event):
        event.ignore()


class DefaultViewerStrategy(ViewerStrategy):
    def __init__(self, parent=None):
        super(DefaultViewerStrategy, self).__init__(parent)

    def can_display_node(self, node):
        return True

    def set_active_node(self, node):
        self.parent().update()

    def set_context_node(self, node):
        self.parent().update()
