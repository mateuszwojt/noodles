import cv2
import numpy as np
from PySide6 import QtWidgets

from OpenGL.GL import *
from OpenGL.GLU import *
from PySide6.QtGui import QPainter
from PySide6.QtOpenGL import QOpenGLTexture

from nodes.image_node import ImageNode
from nodes.node import Node
from render.image_node_renderer import ImageNodeRenderer
from ui.events import PaintEvent
from ui.viewport import Viewport
from ui.viewer.viewer_strategy import ViewerStrategy
from utils.gl_utils import check_gl_errors, clear_gl_errors


class ImageViewerStrategy(ViewerStrategy):
    # widgets
    _viewport = None
    _display = None
    _toolbar = None

    _first_image_loaded = False
    _aspect_ratio = None
    _overlay = None
    _pending_update = False

    _image = None
    _texture = None

    _paint_event = PaintEvent()

    _active_connection = None

    def __init__(self, parent=None):
        super(ImageViewerStrategy, self).__init__(parent)

        self.set_aspect_ratio(1.0)

        self._viewport = Viewport()

    def init(self):
        self._viewport.reset(self.parent().width(), self.parent().height())
        # self._display = GLDisplayManager(self.parent().display_lut())

    def toolbar(self):
        if not self._toolbar:
            self._toolbar = ImageViewToolbar(self)
            self._toolbar.update_widgets(self.visible_node())

        return self._toolbar

    def do_begin_active_view(self):
        self.display_transform_changed()

        node = QtWidgets.QApplication.instance().ui().active_node()
        if node:
            self._active_connection = node.changed.connect(self.active_node_changed)
            # self._active_overlay_connection = node.overlay_changed.connect(self.active_overlay_changed)

        node = QtWidgets.QApplication.instance().ui().context_node()
        if node:
            self._context_connection = node.changed.connect(self.context_node_changed)
            # self._context_overlay_connection = node.overlay_changed.connect(self.context_overlay_changed)

        if self._toolbar:
            self._toolbar.udpate_widgets(self.visible_node())

    def do_end_active_view(self):
        print(self._active_connection)
        print(self._context_connection)

        # TODO: disconnect signals
        self.clear_texture()

    def can_display_node(self, n):
        return isinstance(n, ImageNode)

    def set_active_node(self, node: Node, process: bool):

        if node:
            # TODO: adjust connections
            if process:
                self.active_node_changed()

        if self._toolbar.get():
            self._toolbar.update_widgets(self.visible_node())

    def active_node_changed(self):
        # if not self.parent().view_mode() == view_active_node:
        #     self.parent().update()
        #     return

        if not QtWidgets.QApplication.instance().ui().active_node():
            return

        if not self.parent().autoupdate():
            self._pending_update = True
            self.parent().update()
            return

        self.render_visible_node()

    def active_overlay_changed(self):
        self.parent().update()

    def set_context_node(self, n: Node, process: bool):
        pass

    def context_node_changed(self):
        # if self.parent().view_mode() is not view_context_node

        if not QtWidgets.QApplication.instance().ui().context_node():
            return

        if not self.parent().autoupdate():
            self._pending_update = True
            self.parent().update()
            return

        self.render_visible_node()

    def context_overlay_changed(self):
        pass

    def call_node_changed(self):
        if self.parent().view_mode() == self.parent().ViewMode.VIEW_ACTIVE_NODE:
            self.active_node_changed()
        else:
            self.context_node_changed()

    def visible_node(self):
        if self.parent().view_mode() == self.parent().ViewMode.VIEW_ACTIVE_NODE:
            return QtWidgets.QApplication.instance().ui().active_node()

        return QtWidgets.QApplication.instance().ui().context_node()

    def render_visible_node(self):
        context = QtWidgets.QApplication.instance().document().composition().current_context()
        context.result_node = self.visible_node()

        renderer = ImageNodeRenderer()
        future = QtWidgets.QApplication.instance().ui().render_image(context, renderer)
        if future.done():
            self.load_texture(context.result_node)
            self.parent().update()
            self._pending_update = False
        else:
            QtWidgets.QApplication.instance().ui().error('Out of memory!')

    def load_texture(self, node):
        """This is the actual method that loads image into memory"""
        self.parent().make_current()

        self.set_aspect_ratio(node.aspect_ratio())
        # TODO: get data window
        data_window = None
        self._image.reset(node.image(), node.format(), data_window)

        if self._first_image_loaded:
            # TODO: find center of the new image
            self._viewport.scroll_to_center_point()

    def clear_texture(self):
        pass

    def proxy_level(self):
        return self._proxy_level

    def subsample(self):
        return self._subsample

    def mblur_active(self):
        return self._mblur

    def autoupdate_changed(self):
        if self.parent().autoupdate() and self._pending_update:
            self.call_node_changed()

    def view_mode_changed(self):
        self.call_node_changed()

    def frame_changed(self):
        self.call_node_changed()

    def display_transform_changed(self):
        self.parent().makeCurrent()
        self._display.set_display_transform(self.parent().ocio_config(), self.parent().ocio_transform())
        self._display.set_exposure(self.parent().exposure())
        self.parent().update()

    def exposure_changed(self):
        pass

    def gamma_changed(self):
        pass

    def aspect_toggle(self):
        pass

    def mblur_toggle(self):
        pass

    def change_resolution(self):
        pass

    def change_proxy_level(self):
        pass

    def checks_toggle(self):
        pass

    def overlay_toggle(self):
        pass

    def change_channels(self):
        pass

    def roi_toggle(self):
        pass

    def aspect_ratio(self):
        if self._aspect_ratio:
            return self._aspect_ratio

        return 1.0

    def set_aspect_ratio(self, asp: float):
        self._aspect_ratio = asp

    def pixel_scale(self):
        return self._viewport.zoom_x()

    def screen_to_world(self):
        pass

    def world_to_screen(self):
        pass

    def screen_to_world_dir(self):
        pass

    def save_projection(self):
        pass

    def restore_projection(self):
        pass

    def set_screen_projection(self):
        pass

    def resize(self, x, y):
        pass

    def paint(self):
        pass
        # self._paint_event.view = self
        # self._paint_event.aspect_ratio = self.aspect_ratio()
        # self._paint_event.pixel_scale = self.pixel_scale()
        #
        # clear_gl_errors()
        #
        # glClear(GL_COLOR_BUFFER_BIT)
        #
        # glDisable(GL_TEXTURE_2D)
        # glDisable(GL_TEXTURE_3D)
        #
        # # TODO: use blend
        #
        # # world space
        # glMatrixMode(GL_PROJECTION)
        # glLoadIdentity()
        # glViewport(0, 0, self.parent().width(), self.parent().height())
        # gluOrtho2D(self._viewport.world().min.x, self._viewport.world().max.x,
        #            self._viewport.world().max.y, self._viewport.world().min.y)
        #
        # glMatrixMode(GL_MODELVIEW)
        # glLoadIdentity()
        #
        # glPushMatrix()
        # glScalef(self.subsample() * self.aspect_ratio(), self.subsample())
        #
        # # TODO: draw image background if needed
        #
        # glEnable(GL_TEXTURE_2D)
        # glEnable(GL_TEXTURE_3D)
        #
        # self._display.activate()
        #
        # glColor4f(0, 0, 0, 1)
        # self._image.draw()
        # self._display.deactivate()
        #
        # glDisable(GL_BLEND)
        # glDisable(GL_TEXTURE_2D)
        # glDisable(GL_TEXTURE_3D)
        #
        # # draw red frame
        # glLineWidth(1)
        # glColor4f(0.75, 0.0, 0.0, 1.0)
        # self._image.frame_display_window()
        #
        # if self._overlay:
        #     glColor4f(0.0, 0.0, 0.75, 1.0)
        #     self._image.frame_data_window()
        #
        # glPopMatrix()
        #
        # if self._overlay:
        #     active_node = QtWidgets.QApplication.instance().ui().active_node()
        #
        #     if active_node:
        #         glEnable(GL_BLEND)
        #         glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        #         glEnable(GL_LINE_SMOOTH)
        #
        #         active_node.draw_overlay(self._paint_event)
        #         check_gl_errors()
        #         glDisable(GL_LINE_SMOOTH)
        #         glDisable(GL_BLEND)
        #
        # glFinish()

    def enter_event(self, event):
        pass

    def leave_event(self, event):
        pass

    def key_press_event(self, event):
        pass

    def key_release_event(self, event):
        pass

    def mouse_press_event(self, event):
        pass

    def mouse_move_event(self, event):
        pass

    def mouse_release_event(self, event):
        pass

    def color_at(self, x, y):
        pass

    def pick_colors_in_box(self):
        pass

    def center_image(self):
        pass

    def frame_image(self):
        pass
