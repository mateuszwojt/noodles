from enum import Enum
import logging

from OpenGL.GL import *

from PySide6 import QtOpenGLWidgets, QtCore, QtGui, QtWidgets

from nodes.node import Node
from ui.viewer.viewer_strategy import DefaultViewerStrategy, ViewerStrategy
from ui.viewer.image_view.image_viewer_strategy import ImageViewerStrategy


class ViewerContext(QtOpenGLWidgets.QOpenGLWidget):
    _strategies = []
    _current = None
    _autoupdate = None
    _display_lut = None
    _view_mode = None
    _context = None

    class ViewMode(Enum):
        VIEW_ACTIVE_NODE = 0
        VIEW_CONTEXT_NODE = 1

    def __init__(self, parent=None):
        super(ViewerContext, self).__init__(parent)

        self._first_time = True
        self._autoupdate = True

        self.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.setMouseTracking(True)

        self._view_mode = self.ViewMode.VIEW_ACTIVE_NODE

        # add default viewer strategy, always should go first
        self._strategies.append(DefaultViewerStrategy(self))

        # and other viewer strategies
        self._strategies.append(ImageViewerStrategy(self))

        # set the default current
        self._current = self._strategies[-1]

        self._context = QtGui.QOpenGLContext()

        self._back_color = self.palette().color(QtGui.QPalette.Base)
        self._fg_color = self.palette().color(QtGui.QPalette.ButtonText)

    def view_mode(self):
        return self._view_mode

    def set_view_mode(self, mode: ViewMode):
        if mode is not self._view_mode:
            self._view_mode = mode

            if self._view_mode == self.ViewMode.VIEW_ACTIVE_NODE:
                self.set_strategy_for_node(QtWidgets.QApplication.instance().ui().active_node())
            else:
                self.set_strategy_for_node(QtWidgets.QApplication.instance().ui().context_node())

            self.strategy().view_mode_changed()
            self.update()

    def set_autoupdate(self, flag: bool):
        if self._autoupdate != flag:
            self._autoupdate = flag
            self.strategy().autoupdate_changed()

    def node_added(self, node: Node):
        map(lambda s: s.node_added(node), self._strategies)

    def node_released(self, node: Node):
        map(lambda s: s.node_released(node), self._strategies)

    def set_active_node(self, node):
        if self.view_mode() == self.ViewMode.VIEW_ACTIVE_NODE:
            self.set_strategy_for_node(node)

        for s in self._strategies:
            if s.can_display_node(node):
                s.set_active_node(node, s == self._current and self.view_mode() == self.ViewMode.VIEW_ACTIVE_NODE)
            else:
                s.set_active_node(None)

        if self.view_mode() == self.ViewMode.VIEW_ACTIVE_NODE:
            self.update()

    def set_context_node(self, node):
        if self.view_mode() == self.ViewMode.VIEW_CONTEXT_NODE:
            self.set_strategy_for_node(node)

        for s in self._strategies:
            if s.can_display_node(node):
                s.set_context_node(node, s == self._current and self.view_mode() == self.ViewMode.VIEW_CONTEXT_NODE)
            else:
                s.set_context_node(None)

        if self.view_mode() == self.ViewMode.VIEW_CONTEXT_NODE:
            self.update()

    def set_strategy_for_node(self, node: Node) -> bool:
        match = None
        for strategy in self._strategies:
            if strategy.can_display_node(node):
                match = strategy
                break

        if match is not self._current:
            self.set_strategy(match)
            return True

        return False

    def set_strategy(self, strategy: ViewerStrategy):
        self.strategy().end_active_view()
        strategy.begin_active_view()
        QtWidgets.QApplication.instance().ui().viewer().set_viewer_toolbar(strategy.toolbar())
        self._current = strategy

    def frame_changed(self):
        self.strategy().frame_changed()

    def ocio_config(self):
        pass

    def ocio_transform(self):
        pass

    def display_lut(self):
        pass

    def display_device(self):
        pass

    def display_transform(self):
        pass

    def exposure(self):
        pass

    def gamma(self):
        pass

    def display_transform_changed(self):
        self.strategy().display_transform_changed()

    def exposure_changed(self):
        self.strategy().exposure_changed()

    def gamma_changed(self):
        self.strategy().gamma_changed()

    def cleanup_gl(self):
        self.doneCurrent()

    def initializeGL(self):
        self._context.create()
        # self.context.aboutToBeDestroyed.connect(self.cleanup_gl)

        # initialize functions
        funcs = self._context.functions()
        funcs.initializeOpenGLFunctions()
        funcs.glClearColor(self._back_color.red() / 255.0,
                           self._back_color.green() / 255.0,
                           self._back_color.blue() / 255.0,
                           0)

        if self._first_time:
            self.test_gl_extensions()
            for s in self._strategies:
                s.init()
            self._first_time = False
        else:
            logging.debug('GL context destroyed')

    def resizeGL(self, w, h):
        for s in self._strategies:
            s.resize(w, h)

    def paintGL(self):
        # # Create a QPainter to draw on the widget
        # painter = QtGui.QPainter(self)
        #
        # # Bind the texture to the OpenGL context
        # self.texture.bind()
        #
        # # Draw a textured rectangle covering the whole widget
        # painter.beginNativePainting()
        # self.drawTexture(0, 0, self.width(), self.height(), self.texture)
        # painter.endNativePainting()
        #
        # # Release the texture from the OpenGL context
        # self.texture.release()
        #
        # # End the painter
        # painter.end()

        self.strategy().paint()

    def enterEvent(self, event):
        self.strategy().enter_event(event)

    def leaveEvent(self, event):
        self.strategy().leave_event(event)

    def keyPressEvent(self, event):
        self.strategy().key_press_event(event)

    def keyReleaseEvent(self, event):
        pass

    def mousePressEvent(self, event):
        pass

    def mouseMoveEvent(self, event):
        pass

    def mouseReleaseEvent(self, event):
        pass

    def wheelEvent(self, event):
        pass

    def test_gl_extensions(self):
        self.print_gl_info()

        fmt = self._context.format()

        if self._context.isOpenGLES() or fmt.version() < (2, 0):
            raise RuntimeError('Requires >= OpenGL 2.0 (not ES)')

    def print_gl_info(self):
        info = """
        Vendor: {0}
        Renderer: {1}
        OpenGL Version: {2}
        Shader Version: {3}
        """.format(
            glGetString(GL_VENDOR),
            glGetString(GL_RENDERER),
            glGetString(GL_VERSION),
            glGetString(GL_SHADING_LANGUAGE_VERSION)
        )
        logging.info(info)

    def save_projection(self):
        pass

    def restore_projection(self):
        pass

    def set_screen_projection(self):
        pass

    def draw_checkered_background(self):
        pass

    def color_at(self, x, y):
        return self.strategy().color_at(x, y)

    def strategy(self) -> ViewerStrategy:
        return self._current
