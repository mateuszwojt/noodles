
from utils.constants import QWIDGETSIZE_MAX
from ui.widgets.double_spinbox import DoubleSpinbox
from ui.viewer.viewer_context import ViewerContext
from ui.widgets.container_widget import ContainerWidget
from PySide6 import QtWidgets, QtCore


class Viewer(QtCore.QObject):
    _toolbar_height = 30
    _window = None
    _view = None

    def __init__(self, parent=None):
        super(Viewer, self).__init__(parent)

        # create the OCIO widgets first to init the list of devices and displays
        self._ocio_device_combo = QtWidgets.QComboBox()
        self.get_display_devices()

        self._ocio_transform_combo = QtWidgets.QComboBox()
        self.get_display_transforms()

        self._window = QtWidgets.QWidget()
        self._window.setWindowTitle('Viewer')

        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)

        status_bar = QtWidgets.QWidget()
        status_layout = QtWidgets.QHBoxLayout()
        status_layout.setContentsMargins(0, 0, 0, 0)
        self._status = QtWidgets.QLabel()
        self._status.setText('Status')
        status_layout.addWidget(self._status)
        label_size = self._status.sizeHint()
        status_bar.setMinimumSize(0, label_size.height())
        status_bar.setMaximumSize(QWIDGETSIZE_MAX, label_size.height())

        status_bar.setLayout(status_layout)
        layout.addWidget(status_bar)

        self._view = ViewerContext()
        layout.addWidget(self._view)

        self._active_toolbar = ContainerWidget()
        self._active_toolbar.setMinimumSize(0, self.toolbar_height())
        self._active_toolbar.setMaximumSize(QWIDGETSIZE_MAX, self.toolbar_height())
        self._active_toolbar.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        layout.addWidget(self._active_toolbar)

        separator = QtWidgets.QFrame()
        separator.setFrameStyle(QtWidgets.QFrame.HLine | QtWidgets.QFrame.Raised)
        separator.setLineWidth(1)
        layout.addWidget(separator)

        viewer_controls = QtWidgets.QWidget()
        viewer_controls.setMinimumSize(0, self.toolbar_height())
        viewer_controls.setMaximumSize(QWIDGETSIZE_MAX, self.toolbar_height())
        viewer_controls.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        horizontalLayout = QtWidgets.QHBoxLayout(viewer_controls)
        horizontalLayout.setContentsMargins(0, 0, 0, 0)

        self._result_combo = QtWidgets.QComboBox()
        self._result_combo.insertItems(0, ['Active', 'Context'])
        s = self._result_combo.sizeHint()

        self._update_btn = QtWidgets.QToolButton()
        self._update_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self._update_btn.setCheckable(True)
        self._update_btn.setChecked(True)
        self._update_btn.setText('U')
        self._update_btn.setToolTip('Auto-update')
        self._update_btn.setMaximumWidth(s.height())
        self._update_btn.setMaximumHeight(s.height())
        self._update_btn.toggled.connect(self.autoupdate_toggle)
        horizontalLayout.addWidget(self._update_btn)

        self._result_combo.setFocusPolicy(QtCore.Qt.NoFocus)
        self._result_combo.setToolTip('Show result / context node')
        self._result_combo.activated.connect(self.change_active_context_view)
        horizontalLayout.addWidget(self._result_combo)

        separator = QtWidgets.QFrame()
        separator.setFrameStyle(QtWidgets.QFrame.VLine | QtWidgets.QFrame.Raised)
        separator.setLineWidth(1)
        horizontalLayout.addWidget(separator)

        self._ocio_device_combo.setFocusPolicy(QtCore.Qt.NoFocus)
        self._ocio_device_combo.setToolTip('Display Device')
        self._ocio_device_combo.activated.connect(self.change_display_device)
        horizontalLayout.addWidget(self._ocio_device_combo)

        self._ocio_transform_combo.setFocusPolicy(QtCore.Qt.NoFocus)
        self._ocio_transform_combo.setToolTip('Display Transform')
        self._ocio_transform_combo.activated.connect(self.change_display_transform)
        horizontalLayout.addWidget(self._ocio_transform_combo)

        self._exposure_input = DoubleSpinbox()
        s = self._exposure_input.sizeHint()
        self._exposure_input.setMinimumSize(s)
        self._exposure_input.setMaximumSize(s)
        self._exposure_input.setMinimum(-30)
        self._exposure_input.setMaximum(30)
        self._exposure_input.setSingleStep(0.1)
        self._exposure_input.setDecimals(3)
        self._exposure_input.setToolTip('Viewer Exposure')
        self._exposure_input.valueChanged.connect(self.change_exposure)
        self._exposure_input.spinBoxDragged.connect(self.change_exposure)
        horizontalLayout.addWidget(self._exposure_input)

        self._gamma_input = DoubleSpinbox()
        self._gamma_input.setMinimumSize(s)
        self._gamma_input.setMaximumSize(s)
        self._gamma_input.setMinimum(0)
        self._gamma_input.setMaximum(4)
        self._gamma_input.setValue(1)
        self._gamma_input.setSingleStep(0.1)
        self._gamma_input.setDecimals(3)
        self._gamma_input.setToolTip('Viewer Gamma')
        self._gamma_input.valueChanged.connect(self.change_gamma)
        self._gamma_input.spinBoxDragged.connect(self.change_gamma)
        horizontalLayout.addWidget(self._gamma_input)

        separator = QtWidgets.QFrame()
        separator.setFrameStyle(QtWidgets.QFrame.VLine | QtWidgets.QFrame.Raised)
        separator.setLineWidth(1)
        horizontalLayout.addWidget(separator)

        self._viewer_toolbar = ContainerWidget()
        self._viewer_toolbar.setMinimumSize(0, self.toolbar_height())
        self._viewer_toolbar.setMaximumSize(QWIDGETSIZE_MAX, self.toolbar_height())
        self._viewer_toolbar.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        horizontalLayout.addWidget(self._viewer_toolbar)

        viewer_controls.setLayout(horizontalLayout)
        layout.addWidget(viewer_controls)
        self._window.setLayout(layout)

    def widget(self):
        return self._window

    def width(self):
        return self._view.width()

    def height(self):
        return self._view.height()

    def toolbar_height(self):
        return self._toolbar_height

    def get_display_devices(self):
        pass

    def get_display_transforms(self):
        pass

    def display_device(self):
        pass

    def display_transform(self):
        pass

    def exposure(self):
        pass

    def gamma(self):
        pass

    def update_display_transform(self):
        pass

    def current_viewer(self):
        return self._view.strategy()

    def set_viewer_toolbar(self, widget):
        self._viewer_toolbar.set_contents(widget)

    def set_active_node(self, node):
        pass

    def set_context_node(self, node):
        pass

    def node_added(self, node):
        self._view.node_added(node)

    def node_released(self, node):
        pass

    def frame_changed(self):
        self._view.frame_changed()

    def set_status(self, text):
        self._status.setText(text)

    def begin_interaction(self):
        raise NotImplementedError

    def end_interaction(self):
        raise NotImplementedError

    def autoupdate_toggle(self, state):
        self._view.set_autoupdate(state)

    def change_active_context_view(self, index):
        self._view.set_view_mode(index)  # FIXME: this is wrong, use enum

    def change_display_device(self, index):
        pass

    def change_display_transform(self, index):
        pass

    def change_exposure(self, value):
        self._view.exposure_changed()

    def change_gamma(self, value):
        self._view.gamma_changed()
