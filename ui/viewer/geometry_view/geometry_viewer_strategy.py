from PySide6 import QtWidgets

from ui.viewport import Viewport
from ui.viewer.viewer_strategy import ViewerStrategy


class GeometryViewerStrategy(ViewerStrategy):
    def __init__(self, parent=None):
        super(GeometryViewerStrategy, self).__init__(parent)
