from PySide6 import QtWidgets, QtCore


class TimeControls(QtCore.QObject):
    _window = None

    _stop_playing = True

    def __init__(self, parent=None):
        super(TimeControls, self).__init__(parent)

    def width(self):
        pass

    def height(self):
        pass

    def eventFilter(self, watched: QtCore.QObject, event: QtCore.QEvent) -> bool:
        pass

    def update(self):
        pass

    def goto_start(self):
        pass

    def prev_frame(self):
        pass

    def prev_key(self):
        pass

    def play_back(self):
        pass

    def play_fwd(self):
        pass

    def next_key(self):
        pass

    def next_frame(self):
        pass

    def goto_end(self):
        pass

    def stop_playing(self):
        pass

    def set_autokey(self):
        pass

    def make_flipbook(self):
        pass
