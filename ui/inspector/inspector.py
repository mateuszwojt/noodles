from PySide6 import QtCore, QtWidgets

from ui.widgets.line_edit import LineEdit
from ui.widgets.container_widget import ContainerWidget
from ui.widgets.double_spinbox import DoubleSpinbox


class RenameNodeCommand:
    _node = None

    def __init__(self, node, name, widget):
        self._node = node

    def _rename(self, name):
        self._name_edit.blockSignals(True)
        QtWidgets.QApplication.instance().document().composition().rename_node(self._node, name)


class Inspector(QtCore.QObject):
    _window = None
    _header = None
    _view = None
    _name_edit = None
    _left_margin = None
    _width = None
    _factory = None
    _current = None
    _help = None

    def __init__(self, parent=None):
        super(Inspector, self).__init__(parent)

        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)

        self._window = QtWidgets.QWidget()
        self._window.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        self._window.setMinimumSize(self.width() + 16, 0)
        self._window.setMaximumSize(self.width() + 16, ((1 << 24) - 1))  # use QWidget maximum size limit as second arg
        self._window.setWindowTitle('Inspector')

        scroll = QtWidgets.QScrollArea()
        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setWidgetResizable(True)
        scroll.setContentsMargins(0, 0, 0, 0)

        layout2 = QtWidgets.QVBoxLayout()
        layout2.setContentsMargins(0, 0, 0, 0)

        top = QtWidgets.QWidget()
        self.create_header()
        layout2.addWidget(self._header)

        separator = QtWidgets.QFrame()
        separator.setFrameStyle(QtWidgets.QFrame.HLine | QtWidgets.QFrame.Raised)
        separator.setLineWidth(1)
        layout2.addWidget(separator)

        self._view = ContainerWidget()
        layout2.addWidget(self._view)
        top.setLayout(layout2)
        scroll.setWidget(top)

        layout.addWidget(scroll)
        self._window.setLayout(layout)

        # self._current = self._factory

    def left_margin(self):
        if not self._left_margin:
            tmp_size = QtWidgets.QLabel('M').sizeHint()
            max_label_length = 16
            self._left_margin = tmp_size.width() * max_label_length

        return self._left_margin

    def width(self):
        if not self._width:
            s = DoubleSpinbox().sizeHint()
            self._width = self.left_margin() + 5 + (3 * s.height()) + (3 * s.width()) + 30

        return self._width

    def create_header(self):
        self._header = QtWidgets.QWidget()

        self._name_edit = LineEdit(self._header)

        self._help = QtWidgets.QPushButton(self._header)
        self._help.setText('Help')
        s = self._help.sizeHint()

        hpos = self.left_margin() / 2
        vsize = self._name_edit.sizeHint().height()

        label = QtWidgets.QLabel()
        label.setText('Name')
        label.move(0, 5)
        label.resize(hpos - 5, vsize)
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        self._name_edit.move(hpos, 5)
        self._name_edit.resize(self.width() - hpos - 15 - s.width() - 5, vsize)
        self._name_edit.setEnabled(False)
        self._name_edit.editingFinished.connect(self.rename_node)
        hpos = hpos + self.width() - hpos - 15 - s.width()

        self._help.move(hpos, 5)
        self._help.setEnabled(False)
        self._help.clicked.connect(self.show_help)

        self._header.setMinimumSize(self.width(), vsize + 10)
        self._header.setMaximumSize(self.width(), vsize + 10)
        self._header.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

    def edit_node(self, node=None):
        if QtWidgets.QApplication.instance().quitting():
            return

        # TODO: set current factory

        if node:
            self._current = self._factory.create_panel(node)
            self._view.set_contents(self._current.widget())

        self.update_header_widgets()

    def update(self):
        # TODO:
        pass

    def recreate_panel(self, node):
        self.edit_node()
        self._factory.delete_panel(node)
        self.edit_node(node)

    def update_header_widgets(self):
        self._name_edit.blockSignals(True)

        node = QtWidgets.QApplication.instance().ui().active_node()

        if node:
            self._name_edit.setText(node.name())
            self._name_edit.setEnabled(True)
            self._help.setEnabled(node.help_string())
        else:
            self._name_edit.setText('')
            self._name_edit.setEnabled(False)
            self._help.setEnabled(False)

        self._name_edit.blockSignals(False)

    def rename_node(self):
        if not self._name_edit.isModified():
            return

        node = QtWidgets.QApplication.instance().ui().active_node()
        new_name = self._name_edit.text()

        # TODO: validate new node name string here
        valid = True
        if valid:
            c = RenameNodeCommand(node, new_name, self._name_edit)
            c.redo()
            QtWidgets.QApplication.instance().document().undo_stack().append(c)
            QtWidgets.QApplication.instance().ui().update()
        else:
            self._name_edit.blockSignals(True)
            self._name_edit.setText(node.name())
            self._name_edit.blockSignals(False)

    def show_help(self):
        node = QtWidgets.QApplication.instance().ui().active_node()

        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle('Help')
        msg.setText(node.help_string())
        msg.exec_()

    def widget(self):
        return self._window
