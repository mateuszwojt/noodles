import logging
import os.path
from pathlib import Path

from PySide6 import QtWidgets, QtCore, QtGui

from __version__ import NOODLES_NAME_FULL_VERSION_STR, NOODLES_NAME
from nodes.node_factory import NodeFactory
from ui.node_menu import NodeMenu
from ui.time_controls import TimeControls
from ui.viewer.viewer import Viewer
from ui.compview.composition_view import CompositionView
from ui.widgets.time_slider import TimeSlider

from nodegraph.widgets.node_graph import NodeGraphWidget
from nodegraph.widgets.viewer import NodeViewer

MAX_RECENTLY_OPENED_FILES = 5
DOCUMENT_EXTENSION = '.ndl'
FILE_DIALOG_EXTENSION = 'Noodles Composition (*.ndl)'


class MainWindow(QtWidgets.QMainWindow):
    # actions
    _save = None
    _save_as = None
    _quit = None
    _undo = None
    _redo = None
    _ignore = None
    _delete = None
    _duplicate = None
    _extract = None
    _group = None
    _ungroup = None
    _clear_cache = None
    _preferences = None

    _create_node_actions = {}

    _comp_settings = None
    _comp_flipbook = None
    _comp_render = None
    _comp_view = None
    _time_controls = None
    _node_menus = []

    # menus
    _view = None

    _recently_opened = None

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self._menubar = self.menuBar()

        self._time_controls = TimeControls()

        self.node_factory = NodeFactory()
        logging.info('Registered nodes: %s' % self.node_factory.registered_nodes())

        # register actions and menus
        self._create_actions()
        self._create_menus()

        # docks
        self.setCorner(QtCore.Qt.TopRightCorner, QtCore.Qt.RightDockWidgetArea)
        self.setCorner(QtCore.Qt.BottomRightCorner, QtCore.Qt.RightDockWidgetArea)

        # inspector
        self._inspector_dock = QtWidgets.QDockWidget('Inspector')
        self._inspector_dock.setObjectName('inspector_dock')
        self._inspector_dock.setAllowedAreas(QtCore.Qt.RightDockWidgetArea | QtCore.Qt.LeftDockWidgetArea |
                                             QtCore.Qt.BottomDockWidgetArea)
        self._inspector_dock.setWidget(QtWidgets.QApplication.instance().ui().inspector())
        self._add_dock_widget(QtCore.Qt.RightDockWidgetArea, self._inspector_dock)

        # anim editor dock

        # composition view
        self._composition_dock = QtWidgets.QDockWidget('Composition')
        self._composition_dock.setObjectName('composition_dock')
        self._composition_dock.setAllowedAreas(QtCore.Qt.BottomDockWidgetArea | QtCore.Qt.TopDockWidgetArea |
                                               QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)

        # Composition View
        all_comp_view = NodeGraphWidget()
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)

        self._comp_view = NodeViewer()
        layout.addWidget(self._comp_view)

        separator = QtWidgets.QFrame()
        separator.setFrameStyle(QtWidgets.QFrame.HLine | QtWidgets.QFrame.Raised)
        separator.setLineWidth(1)
        layout.addWidget(separator)

        # layout.addWidget(self.composition_view().create_toolbar())
        all_comp_view.setLayout(layout)

        self._composition_dock.setWidget(all_comp_view)
        self._add_dock_widget(QtCore.Qt.LeftDockWidgetArea, self._composition_dock)

        # Main Viewer
        self.setCentralWidget(QtWidgets.QApplication.instance().ui().viewer().widget())

        # Time Toolbar
        self.addToolBar(QtCore.Qt.BottomToolBarArea, self._create_time_toolbar())

        # Main Status Bar
        self.statusBar().showMessage(NOODLES_NAME_FULL_VERSION_STR)

        # resize and center
        center = QtGui.QScreen.availableGeometry(QtWidgets.QApplication.primaryScreen()).center()
        geo = self.frameGeometry()
        geo.moveCenter(center)
        self.move(geo.topLeft())
        self.resize(geo.width(), geo.height() - 40)
        self.setWindowTitle(NOODLES_NAME)

        # TODO: set window icon

    def _create_time_toolbar(self):
        toolbar = QtWidgets.QToolBar('Time Controls')
        toolbar.setObjectName('time_controls')
        toolbar.setFloatable(False)
        toolbar.setMovable(False)

        self._time_slider = TimeSlider()
        self._time_slider.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                        self._time_slider.sizePolicy().verticalPolicy())
        self._time_slider.start_frame_changed.connect(QtWidgets.QApplication.instance().ui().set_start_frame)
        self._time_slider.end_frame_changed.connect(QtWidgets.QApplication.instance().ui().set_end_frame)
        self._time_slider.time_changed.connect(QtWidgets.QApplication.instance().ui().set_frame)

        toolbar.addWidget(self._time_slider)
        toolbar.addSeparator()

        # TODO: add time controls
        # toolbar.addWidget()
        return toolbar

    def _create_actions(self):
        logging.debug('creating actions')

        self._new = QtGui.QAction('New')
        self._new.setShortcut('Ctrl+N')
        self._new.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._new.triggered.connect(self.new_document)

        self._open = QtGui.QAction('Open...')
        self._open.setShortcut('Ctrl+O')
        self._open.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._open.triggered.connect(self.open_document)

        # for (int i = 0 i < max_recently_opened_files ++i)
        # {
        #     recently_opened_[i] = new
        # QAction()
        # recently_opened_[i].setVisible(false)
        # connect(recently_opened_[i], SIGNAL(triggered()), , SLOT(open_recent_document()))
        # }

        self._save = QtGui.QAction('Save')
        self._save.setShortcut('Ctrl+S')
        self._save.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._save.triggered.connect(self.save_document)

        self._save_as = QtGui.QAction('Save As...')
        self._save_as.triggered.connect(self.save_document_as)

        self._quit = QtGui.QAction('Quit')
        self._quit.setShortcut('Ctrl+Q')
        self._quit.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._quit.triggered.connect(self.quit)

        # import_comp_ = QAction('Import Composition...', )
        # connect(import_comp_, SIGNAL(triggered()), , SLOT(import_composition()))
        #
        # export_sel_ = new
        # QAction('Export Selection...', )
        # export_sel_.setEnabled(false)
        # connect(export_sel_, SIGNAL(triggered()), , SLOT(export_selection()))

        self._undo = QtGui.QAction('Undo')
        self._undo.setShortcut('Ctrl+Z')
        self._undo.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._undo.triggered.connect(self.undo)

        self._redo = QtGui.QAction('Redo')
        self._redo.setShortcut('Ctrl+Shift+Z')
        self._redo.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._redo.triggered.connect(self.redo)

        self._ignore = QtGui.QAction('Ignore')
        self._ignore.setShortcut('Ctrl+I')
        self._ignore.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._ignore.triggered.connect(self.ignore_nodes)

        self._delete = QtGui.QAction('Delete')
        self._delete.triggered.connect(self.delete_nodes)

        self._duplicate = QtGui.QAction('Duplicate')
        self._duplicate.setShortcut('Ctrl+D')
        self._duplicate.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._duplicate.triggered.connect(self.duplicate_nodes)

        self._extract = QtGui.QAction('Extract')
        self._extract.triggered.connect(self.extract_nodes)

        # TODO:
        self._group = QtGui.QAction('Group')
        self._ungroup = QtGui.QAction('Ungroup')

        self._clear_cache = QtGui.QAction('Clear Image Cache')
        self._clear_cache.triggered.connect(self.clear_cache)

        self._preferences = QtGui.QAction('Preferences...')
        self._preferences.triggered.connect(self.show_preferences_dialog)

        self._comp_settings = QtGui.QAction('Composition Settings...')
        self._comp_settings.setShortcut('Ctrl+K')
        self._comp_settings.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._comp_settings.triggered.connect(self.show_composition_settings_dialog)

        self._comp_flipbook = QtGui.QAction('Render Flipbook...')
        self._comp_flipbook.triggered.connect(self.render_flipbook)

        self._comp_render = QtGui.QAction('Render Composition...')
        self._comp_render.setShortcut('Ctrl+R')
        self._comp_render.setShortcutContext(QtCore.Qt.ApplicationShortcut)
        self._comp_render.triggered.connect(self.render_composition)

        self._about = QtGui.QAction('About Noodles...')
        self._about.triggered.connect(self.show_about_box)

        self._project_web = QtGui.QAction(self.tr('Project Website...'))
        self._project_web.triggered.connect(self.go_to_project_website)

        # non-menu actions
        self._next_frame = QtGui.QAction()
        self._next_frame.setShortcut(QtGui.QKeySequence.MoveToNextChar)
        self._next_frame.setShortcutContext(QtCore.Qt.WidgetWithChildrenShortcut)
        self._next_frame.triggered.connect(self._time_controls.next_frame)
        self.addAction(self._next_frame)

        self._prev_frame = QtGui.QAction()
        self._prev_frame.setShortcut(QtGui.QKeySequence.MoveToPreviousChar)
        self._prev_frame.setShortcutContext(QtCore.Qt.WidgetWithChildrenShortcut)
        self._prev_frame.triggered.connect(self._time_controls.prev_frame)
        self.addAction(self._prev_frame)

    def _create_menus(self):
        logging.debug('creating menus')

        self._file = self._menubar.addMenu('File')
        self._file.addAction(self._new)
        self._file.addAction(self._open)

        open_recent_ = self._file.addMenu('Open Recent')
        # for i in range(0, MAX_RECENTLY_OPENED_FILES + 1):
        #     open_recent_.addAction(self._recently_opened[i])

        self.init_recent_files_menu()

        self._file.addAction(self._save)
        self._file.addAction(self._save_as)
        self._file.addSeparator()

        import_ = self._file.addMenu('Import')
        export_ = self._file.addMenu('Export')
        self._create_import_export_menus()

        self._file.addSeparator()
        self._file.addAction(self._quit)

        self._edit = self._menubar.addMenu('Edit')
        self._edit.addAction(self._undo)
        self._edit.addAction(self._redo)
        self._edit.addSeparator()
        self._edit.addAction(self._ignore)
        self._edit.addAction(self._delete)
        self._edit.addAction(self._duplicate)
        self._edit.addAction(self._extract)
        self._edit.addSeparator()
        self._edit.addAction(self._group)
        self._edit.addAction(self._ungroup)
        self._edit.addSeparator()
        self._edit.addAction(self._clear_cache)
        self._edit.addSeparator()
        self._edit.addAction(self._preferences)

        self._composition = self._menubar.addMenu('Composition')
        self._composition.addAction(self._comp_settings)
        self._composition.addSeparator()
        self._composition.addAction(self._comp_flipbook)
        self._composition.addAction(self._comp_render)

        self.create_node_actions()

        for m in self._node_menus:
            m.menu().setTearOffEnabled(True)
            self._menubar.addMenu(m.menu())

        self._view = self._menubar.addMenu('Window')

        help_ = self._menubar.addMenu('Help')
        help_.addAction(self._about)
        help_.addAction(self._project_web)

    def _add_dock_widget(self, area: QtCore.Qt.DockWidgetArea, dock: QtWidgets.QDockWidget):
        self.addDockWidget(area, dock)
        self._view.addAction(dock.toggleViewAction())

    def _find_node_menu(self, s) -> NodeMenu:
        for idx, _ in enumerate(self._node_menus):
            if self._node_menus[idx].name() == s:
                return self._node_menus[idx]

        menu = NodeMenu(s)
        self._node_menus.append(menu)
        return menu

    def create_node_actions(self):
        m = NodeMenu('Image')
        self._node_menus.append(m)
        m.add_submenu('Input')
        m.add_submenu('Channels')
        m.add_submenu('Color')
        m.add_submenu('Key')
        m.add_submenu('Matte')
        m.add_submenu('Filter')
        m.add_submenu('Noise')
        m.add_submenu('Distort')
        m.add_submenu('Transform')
        m.add_submenu('Track')
        m.add_submenu('Layer')
        m.add_submenu('Lighting')
        m.add_submenu('Tonemap')
        m.add_submenu('Time')
        m.add_submenu('Util')
        m.add_submenu('Output')

        # node_factory.sort_by_menu_item()

        # for metaclass in self.node_factory.registered_nodes():
        #     # if metaclass.ui_visible and self.node_factory.is_latest_version(metaclass.id):
        #     menu = self._find_node_menu(metaclass.menu)
        #     act = QtGui.QAction(metaclass.menu_item, self)
        #     act.triggered.connect(self.create_node)
        #     self._create_node_actions[act] = metaclass.id
        #     menu.add_action(metaclass.submenu, act)

    def _create_import_export_menus(self):
        pass

    def _can_close_document(self):
        if QtWidgets.QApplication.instance().document().dirty():
            # composition modified, show warning
            ret = QtWidgets.QMessageBox.warning(self, "The composition has been modified.\n"
                                                "Do you want to save your changes?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Default,
                                                QtWidgets.QMessageBox.No,
                                                QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Escape)
            if ret == QtWidgets.QMessageBox.Yes:
                self.save_document()
                return QtWidgets.QApplication.instance().document().dirty()
            elif ret == QtWidgets.QMessageBox.Cancel:
                return False

        return True

    def new_document(self):
        if self._can_close_document():
            QtWidgets.QApplication.instance().ui().create_new_document()

    def open_document(self):
        if self._can_close_document():
            filename, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Composition", "", FILE_DIALOG_EXTENSION,
                                                                "", QtWidgets.QFileDialog.DontUseNativeDialog)
            print(f'filename: {filename}')
            if filename:
                QtWidgets.QApplication.instance().ui().open_document(filename)

    def save_document(self):
        if QtWidgets.QApplication.instance().document().has_file():
            QtWidgets.QApplication.instance().ui().save_document()
            QtWidgets.QApplication.instance().ui().update()
        else:
            self.save_document_as()

    def save_document_as(self):
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Composition As', '', FILE_DIALOG_EXTENSION,
                                                            options=QtWidgets.QFileDialog.DontUseNativeDialog)

        if filename:
            # force replace filename extension
            filename = Path(filename).with_suffix(DOCUMENT_EXTENSION)

            old_file = QtWidgets.QApplication.instance().document().file()
            QtWidgets.QApplication.instance().document().set_file(filename)

            if not QtWidgets.QApplication.instance().ui().save_document():
                if old_file:
                    QtWidgets.QApplication.instance().document().set_file(old_file)
            else:
                self.update_recent_files_menu(QtWidgets.QApplication.instance().document().file())
                # QtWidgets.QApplication.instance().document().undo_stack().clear_all()

            QtWidgets.QApplication.instance().ui().update()

    def quit(self):
        if QtWidgets.QApplication.instance().document().dirty():
            ret = QtWidgets.QMessageBox.warning(self, "The composition has been modified.\n"
                                                "Do you want to save your changes before quitting?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Default,
                                                QtWidgets.QMessageBox.No,
                                                QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Escape)
            if ret == QtWidgets.QMessageBox.Yes:
                self.save_document()
                # if the document is still dirty, it means
                # save was cancelled, so we return without quitting
                if QtWidgets.QApplication.instance().document().dirty():
                    return
            elif ret == QtWidgets.QMessageBox.No:
                QtWidgets.QApplication.instance().ui().quit()
            else:
                return

        QtWidgets.QApplication.instance().ui().quit()

    def undo(self):
        QtWidgets.QApplication.instance().document().undo_stack().undo()
        QtWidgets.QApplication.instance().ui().update()

    def redo(self):
        QtWidgets.QApplication.instance().document().undo_stack().redo()
        QtWidgets.QApplication.instance().ui().update()

    def ignore_nodes(self):
        pass

    def delete_nodes(self):
        pass

    def duplicate_nodes(self):
        pass

    def extract_nodes(self):
        pass

    def clear_cache(self):
        pass

    def show_preferences_dialog(self):
        pass

    def show_composition_settings_dialog(self):
        pass

    def render_flipbook(self):
        pass

    def render_composition(self):
        pass

    def show_about_box(self):
        msg_box = QtWidgets.QMessageBox()
        msg_box.setTextFormat(QtCore.Qt.RichText)
        msg_box.setWindowTitle("About Noodles")
        msg_box.setText(("<html><p style=\" font-size:16pt;"
                        "font-weight:bold;\">%s</p></html>") % NOODLES_NAME_FULL_VERSION_STR)
        msg_box.setInformativeText("For full license details please read the LICENSE.txt file")

        spacer = QtWidgets.QSpacerItem(400, 0, QtWidgets.QSizePolicy.Minimum,
                                                 QtWidgets.QSizePolicy.Expanding)
        layout = msg_box.layout()
        layout.addItem(spacer, layout.rowCount(), 0, 1, layout.columnCount())

        msg_box.exec()

    def go_to_project_website(self):
        """Open project website in web browser"""
        QtGui.QDesktopServices.openUrl(QtCore.QUrl('https://gitlab.com/mateuszwojt/noodles'))

    def create_node(self):
        sender = self.sender()
        id_ = self._create_node_actions[sender]
        version = (1, 0)

        NodeFactory().create_by_id_with_version(id_, version)

    def update(self):
        # Update window title based on the document state
        if QtWidgets.QApplication.instance().document().dirty():
            self.setWindowTitle("Noodles *")
        else:
            self.setWindowTitle("Noodles")

        self.update_menus()

        self.time_slider().update(QtWidgets.QApplication.instance().document().composition().start_frame(),
                                  QtWidgets.QApplication.instance().document().composition().frame(),
                                  QtWidgets.QApplication.instance().document().composition().end_frame())

        self.composition_view().update()
        self._time_controls.update()

    def init_recent_files_menu(self):
        settings = QtCore.QSettings('com.mateuszwojt.noodles', 'Noodles Recent Files')
        files = settings.value('recent_file_list')
        if not files:
            return

        num_recent_files = min(len(files), MAX_RECENTLY_OPENED_FILES)

        for i in range(num_recent_files):
            stripped = QtCore.QFileInfo(files[i]).fileName()
            text = f'{i + 1} {stripped}'
            self._recently_opened[i].setText(text)
            self._recently_opened[i].setData(files[i])
            self._recently_opened[i].setVisible(True)

        for j in range(num_recent_files, MAX_RECENTLY_OPENED_FILES):
            self._recently_opened[j].setVisible(False)

    def update_recent_files_menu(self, path):
        filename = os.path.basename(path)

        settings = QtCore.QSettings('com.mateuszwojt.noodles', 'Noodles Recent Files')
        files = settings.value('recent_file_list')
        try:
            files.remove(filename)
        except ValueError:
            return

        # update list of recent filenames with new entry as the first element
        # and trim the list back to the limit
        files.prepend(filename)
        files = files[:MAX_RECENTLY_OPENED_FILES]
        settings.setValue('recent_file_list', files)

        num_recent_files = min(len(files), MAX_RECENTLY_OPENED_FILES)

        for i in range(num_recent_files):
            stripped = QtCore.QFileInfo(files[i]).fileName()
            text = f'{i + 1} {stripped}'
            self._recently_opened[i].setText(text)
            self._recently_opened[i].setData(files[i])
            self._recently_opened[i].setVisible(True)

        for j in range(num_recent_files, MAX_RECENTLY_OPENED_FILES):
            self._recently_opened[j].setVisible(False)

    def update_menus(self):
        pass

    def composition_view(self):
        return self._comp_view

    def time_slider(self) -> TimeSlider:
        return self._time_slider
