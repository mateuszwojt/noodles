from PySide6 import QtWidgets

from datatypes.box import Box2i, Box2f


class Viewport(QtWidgets.QGraphicsView):
    _device = Box2i(1, 1)
    _world = Box2f(1, 1)
    _y_down = None

    def __init__(self, parent=None):
        super(Viewport, self).__init__(parent)
        self._y_down = False

    def zoom_x(self):
        return self._device.size()

    def zoom_y(self):
        pass

    def reset(self, w, h):
        pass

    def zoom(self, center, factor):
        pass

    def world(self):
        return self._world

    def device(self):
        return self._device
