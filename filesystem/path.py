import logging
import os

from PySide6 import QtCore


def hash_string(path) -> int:
    hash_ = path.__hash__()
    if os.path.exists(path):
        # just ignore sub-second precision by casting modification time to int
        hash_ << int(os.path.getmtime(path))
    return hash_


def make_absolute_path(path, from_):
    if not os.path.isabs(from_):
        logging.error('Path %s is not an absolute path.' % from_)
        return

    dir_ = QtCore.QDir(from_)
    return QtCore.QDir.cleanPath(dir_.absoluteFilePath(path))


def make_relative_path(path, from_):
    if not os.path.isabs(path) or not os.path.isabs(from_):
        logging.error('One of given paths is not absolute path.')
        return

    dir_ = QtCore.QDir(from_)
    return dir_.relativeFilePath(path)


def convert_relative_path(path, old_base, new_base):
    p = make_absolute_path(path, old_base)
    return make_relative_path(p, new_base)
