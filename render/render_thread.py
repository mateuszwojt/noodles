from render.image_node_renderer import ImageNodeRenderer


class RenderThread:
    def __init__(self):
        pass

    def init(self):
        pass

    def render_image(self, renderer, roi=None):
        roi = roi or renderer.format()
        ImageNodeRenderer.render(roi)
