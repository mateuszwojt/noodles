from enum import Enum


class RenderMode(Enum):
    INTERFACE_RENDER = 1
    FLIPBOOK_RENDER = 2
    PROCESS_RENDER = 3
    ANALISYS_RENDER = 4


class Context:
    def __init__(self):
        self.mode = RenderMode.INTERFACE_RENDER
        self.frame = 1
        self.subsample = 1
        self.composition = 0
        self.result_node = 0
        self.proxy_level = 0
        self.motion_blur_extra_samples = 0
        self.motion_blur_shutter_factor = 0
        self.cancel = self.default_cancel
        self.error_log = 0

    def render_cancelled(self):
        if callable(self.cancel):
            return self.cancel()
        return False

    def default_cancel(self):
        return False
