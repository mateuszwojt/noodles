


class ImageNodeRenderer:
    _new_context = None
    _has_context = False
    _n = None
    _render_done = None

    def __init__(self, new_context=None):
        if new_context:
            self.set_context(new_context)

    def set_context(self, context):
        self._has_context = True
        self._new_context = context
        self._n = self._new_context.result_node
        self._n.release_image()
        self._n.composition().set_frame(self._new_context.frame)

    def format(self):
        if not self._has_context:
            return

        return self._n.format()

    def bounds(self):
        if not self._has_context:
            return

        return self._n.bounds()

    def render(self, roi=None):
        if not roi:
            roi = self._n.format()

        if roi.isEmpty():
            # TODO: debug log?
            return

        self._n.set_interest(roi)
        # TODO: traverse graph

        try:
            self._n.recursive_process(self._new_context)
            self._render_done = True
        except Exception as e:
            # not sure what can happen here
            raise RuntimeError(e)

    def image(self):
        if not self._has_context:
            return

        return self._n.image()

    def format_image_view(self):
        if not self._has_context:
            return

        return self._n.const_subimage_view(self.format())
