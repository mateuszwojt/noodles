import logging
import sys

from app import application


def _setup_logging():
    # debug logging to file
    format_ = '[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s'
    datefmt = '%H:%M:%S'
    logging.basicConfig(filename='noodles.log'.format(__name__),
                        level=logging.DEBUG,
                        format=format_,
                        datefmt=datefmt,
                        )

    # setup simpler logging to console
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)


def main():
    _setup_logging()

    app = application.Application(sys.argv)
    app.run()

    sys.exit(app.exec())


if __name__ == '__main__':
    main()
