import logging
import os.path
import signal

from typing import Tuple

from PySide6 import QtCore, QtWidgets

from filesystem.path import make_relative_path, make_absolute_path
from serialization.yaml_iarchive import YamlIArchive
from serialization.yaml_oarchive import YamlOArchive
from nodes.node import Node
from nodes.node_factory import NodeFactory

from nodegraph import NodeGraph


class Composition(QtCore.QObject):
    _start_frame = 1
    _end_frame = 100
    _frame = 1
    _default_format = None
    _frame_rate = None
    _autokey = True

    _node_map = None
    _graph = None

    _added = QtCore.Signal(object)
    _released = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(Composition, self).__init__(parent)

        self.init()

    def init(self):
        ui = QtWidgets.QApplication.instance().ui()
        if not ui:
            return

        main_window = ui.main_window()
        if not main_window:
            return

        comp_viewer = main_window.composition_view()
        inspector = ui.inspector()
        undo_stack = self.parent().undo_stack()

        # Initialize new graph controller
        self._graph = NodeGraph(undo_stack=undo_stack, viewer=comp_viewer, node_factory=NodeFactory())
        inspector.wire_node_graph(self._graph)

    def attach_add_observer(self, subscriber):
        return self._added.emit(subscriber)

    def attach_release_observer(self, subscriber):
        return self._released.connect(subscriber)

    def add_node(self, node: Node):
        self._graph.add_node(node)

    def release_node(self, node):
        pass

    def find_node(self, name) -> Node:
        pass

    def add_edge(self, edge, notify):
        pass

    def remove_edge(self, edge, notify):
        pass

    def merge_composition(self, other_comp):
        pass

    def rename_node(self, node, new_name):
        pass

    def notify_all_dirty(self):
        pass

    def clear_all_notify_dirty_flags(self):
        pass

    def begin_interaction(self):
        pass

    def end_interaction(self):
        pass

    def can_connect(self, src: Node, dst: Node, port: int) -> bool:
        pass

    def connect(self, src: Node, dst: Node, port: int):
        pass

    def disconnect(self, src, dst, port):
        pass

    def frame(self):
        return self._frame

    def set_frame(self, frame):
        pass

    def start_frame(self):
        return self._start_frame

    def end_frame(self):
        return self._end_frame

    def set_start_frame(self, f):
        self._start_frame = f

    def set_end_frame(self, f):
        self._end_frame = f

    def current_context(self, mode=None):
        pass

    def select_all(self):
        self._graph.select_all()

    def deselect_all(self):
        pass

    def any_selected(self):
        pass

    def selected_node(self):
        self._graph.selected_nodes()

    def composition_dir(self):
        pass

    def set_composition_dir(self, dir):
        pass

    def convert_all_relative_paths(self, new_base):
        pass

    def make_all_paths_absolute(self):
        pass

    def make_all_paths_relative(self):
        pass

    def relative_to_absolute(self, path):
        return make_absolute_path(path, self.composition_dir())

    def absolute_to_relative(self, path):
        return make_relative_path(path, self.composition_dir())

    def load_from_file(self, path):
        if not os.path.isabs(path):
            logging.error('Path %s is not an absolute path.' % path)
            return

        archive = YamlIArchive(path)
        if not archive.read_composition_header():
            logging.error('Could not open file %s' % path)
            return

        self.set_composition_dir(os.path.dirname(path))
        self.read(archive)

    def read(self, archive: YamlIArchive):
        self._start_frame = archive.get_value('start_frame')
        self._end_frame = archive.get_value('end_frame')
        self._frame = archive.get_value('frame')
        self._frame_rate = archive.get_value('frame_rate')
        self._autokey = archive.get_value('autokey')
        self._default_format = archive.get_value('format')

        nodes_layout = archive.get_value('nodes')
        self.graph().deserialize_session(nodes_layout)

    def read_nodes(self, archive: YamlIArchive):
        nodes = archive.get_value('nodes')
        for n in nodes:
            self.read_node(n)

    def read_node(self, node: dict):
        id_, version = node.get('class')
        new_node = self.create_node(id_, version)
        if not new_node:
            logging.error('Error creating node %s' % id_)
            return

        # new_node.set_composition(self)
        # new_node.read(node, version)
        # new_node.set_frame(self._frame)

        # if isinstance(p, ImageNode):
        #     context = self.current_context()
        #     p.calc_format(context)
        #     p.format_changed()

        self._node_map.append(new_node)
        self.add_node(new_node)

    def create_node(self, id_: str, version: Tuple[int, int]):
        node = NodeFactory().create_by_id_with_version(id_, version)

        # as a last resort, return an unknown node
        if not node:
            return self.create_unknown_node(id_, version)

        try:
            print(f'Adding new node: {node}')
            self.add_node(node)
            # node.set_composition(self)
            # node.create_params()
            # node.create_manipulators()
        except Exception as e:
            logging.error('Error while creating node %s: %s' % (id_, e))
            return self.create_unknown_node(id_, version)

        return node

    def create_unknown_node(self, id_, version):
        logging.error('Creating unknown node is not implemented yet')
        return Node()

    def write(self):
        data = {
            'start_frame': self._start_frame,
            'end_frame': self._end_frame,
            'frame': self._frame,
            'frame_rate': self._frame_rate,
            'autokey': self._autokey,
            'format': self._default_format,
            'nodes': self.serialized_nodes(),
        }
        return YamlOArchive(data)

    def serialized_nodes(self):
        return self.graph().serialize_session()

    def nodes(self):
        return self._graph.all_nodes()

    def graph(self):
        return self._graph
