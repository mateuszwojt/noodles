import logging
import sys

from PySide6 import QtWidgets

import system
from __version__ import NOODLES_NAME_FULL_VERSION_STR
from app.document import Document
from render.render_thread import RenderThread
from serialization.yaml_iarchive import YamlIArchive
from ui.user_interface import UserInterface
from ui.dialogs.splash_screen import SplashScreen


class Application(QtWidgets.QApplication):
    _max_threads = 0
    _img_cache_size = 0
    _command_line = False
    _render_mode = False
    _quitting = False

    _document = None
    _preferences = None
    _render_thread = None

    _input_file = None

    def __init__(self, *args, **kwargs):
        super(Application, self).__init__()

        # create the application user path, if needed

        # parse command line

        # init prefs
        # self._preferences.reset()

        self._splash = SplashScreen()

        if not self._command_line:
            # self._splash.reset()
            self._splash.show()
            self._splash.show_message(NOODLES_NAME_FULL_VERSION_STR)

        # TODO: init memory manager

        if not self._command_line:
            self._splash.show_message('Initializing builtin nodes')
        # TODO: init nodefactory instance

        if not self._command_line:
            self._splash.show_message('Initializing image processing')
        # TODO: init imageprocessing instance

        if not self._command_line:
            self._splash.show_message('Initializing movie playback')
        # TODO: init movieio instance

        if not self._command_line:
            self._splash.show_message('Initializing OpenColorIO')
        # TODO: init OCIO instance

        if not self._command_line:
            self._splash.show_message('Initializing render thread')
        self._render_thread = RenderThread()
        self._render_thread.init()

        if not self._command_line:
            self._splash.show_message('Initializing user interface')
            self._ui = UserInterface()
            self._ui.init()
            self.print_app_info()

    def run(self):
        if not self._command_line:
            self._ui.show()
            self._splash.finish(self._ui.main_window())
            # return self._ui.run(self._input_file)
        else:
            if self._render_mode:
                try:
                    self.open_document(self._input_file)
                except Exception as e:
                    raise RuntimeError('Error when opening document %s: %s' % (self._input_file, e))

            # TODO: run render

    def print_app_info(self):
        # TODO: add image cache size info
        info = """
        Noodles
        System: {0}
        Executable: {1}
        App dir: {2}
        App user dir: {3}
        Using {4} threads
        Ram Size: {5} Mb
        """.format(
            system.system_name(),
            system.executable_path(),
            system.application_path(),
            system.application_user_path(),
            self._max_threads,
            (system.ram_size() / 1024 / 1024),
        )
        logging.info(info)

    def create_new_document(self):
        self.delete_document()
        self._document = Document(self)

    def open_document(self, path: str):
        self.create_new_document()
        archive = YamlIArchive(path)
        if not archive.read_composition_header():
            raise RuntimeError('Cannot read file header %s' % path)

        self.document().set_file(path)
        self.document().load(archive)

    def delete_document(self):
        pass

    def fatal_error(self, msg, no_gui=False):
        if not self._command_line and self._ui and not self._ui.rendering() and not no_gui:
            self._ui.fatal_error(msg)
        else:
            logging.fatal('Fatal error: %s' % msg)

        sys.exit(1)

    def error(self, msg, no_gui=False):
        if not self._command_line and self._ui and not self._ui.rendering() and not no_gui:
            self._ui.fatal_error(msg)
        else:
            logging.error('Error: %s' % msg)

    def inform(self, msg, no_gui=False):
        if not self._command_line and self._ui and not self._ui.rendering() and not no_gui:
            self._ui.inform(msg)
        else:
            logging.info('Info: %s' % msg)

    def question(self, what, default_answer=False):
        if not self._command_line and self._ui and not self._ui.rendering():
            self._ui.question(what, default_answer)
        else:
            raise NotImplementedError

        return default_answer

    def ui(self):
        return self._ui

    def document(self):
        return self._document

    def render_thread(self):
        return self._render_thread

    def quitting(self):
        return self._quitting
