import os.path

from PySide6 import QtCore, QtGui

from app.composition import Composition
from serialization.yaml_iarchive import YamlIArchive
from serialization.yaml_oarchive import YamlOArchive


class Document(QtCore.QObject):
    _dirty = None
    _undo = None
    _file = None
    _comp = None

    def __init__(self, parent=None):
        super(Document, self).__init__(parent)

        # Initialize undo stack
        self._undo = QtGui.QUndoStack(self)

        # Initialize new Composition
        self._comp = Composition(self)

    def dirty(self):
        return self._dirty

    def set_dirty(self, b: bool):
        self._dirty = b

    def undo_stack(self):
        return self._undo

    def has_file(self):
        return self._file is not None

    def file(self):
        return self._file

    def set_file(self, path):
        if not os.path.isabs(path):
            raise OSError('Filepath %s is not an absolute path.' % path)
        if path == '':
            raise RuntimeError('Filepath cannot be empty')

        self._file = path
        self.composition().set_composition_dir(os.path.dirname(path))

    def load(self, archive: YamlIArchive):
        if not self.has_file():
            raise RuntimeError('File not set, cannot load')

        self.composition().read(archive)

    def save(self) -> YamlOArchive:
        return self.composition().write()

    def composition(self) -> Composition:
        return self._comp

