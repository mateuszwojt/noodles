

class MovieIOException(Exception):
    pass


class FrameOutOfBounds(MovieIOException):
    pass
