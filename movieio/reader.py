import os.path

import filesystem.path

import movieio.exceptions


class Reader:
    _frame = 0
    _path = None
    _info = None
    _channels = None

    def is_sequence(self):
        return True

    def pad(self):
        return 0

    def format_string(self):
        return os.path.abspath(self._path)

    def string_for_current_frame(self):
        hash_str = str(filesystem.path.hash_string(self._path))

        if self.is_sequence():
            hash_str += '-{}'.format(self._frame)

        return hash_str

    def movie_info(self):
        return self._info

    def format(self):
        return self._info.get('format')

    def bounds(self):
        return self._info.get('bounds')

    def aspect_ratio(self):
        return self._info.get('aspect', default=1.0)

    def is_multichannel(self):
        return len(self._channels) > 0

    def has_extra_channels(self):
        return False

    def channel_list(self):
        return self._channels

    def start_frame(self):
        raise NotImplementedError

    def end_frame(self):
        raise NotImplementedError

    def set_frame(self, frame):
        if self.is_sequence():
            if frame >= self.start_frame() and frame <= self.end_frame():
                self._frame = frame
                self.do_set_frame(frame)
            else:
                raise movieio.exceptions.FrameOutOfBounds

    def do_set_frame(self, frame: int):
        raise NotImplementedError

    def read_frame(self):
        pass

    def do_read_frame(self):
        raise movieio.exceptions.MovieIOException('Movie format does not support multichannel reading')
