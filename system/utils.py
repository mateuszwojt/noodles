import cProfile
import pstats
import io


def profile_it(func):
    def wrapper(*args, **kwargs):
        prof = cProfile.Profile()
        prof.enable()
        retval = prof.runcall(func, *args, **kwargs)
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(prof, stream=s).sort_stats(sortby)
        ps.print_stats()
        return retval

    return wrapper
