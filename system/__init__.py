import getpass
import os
import platform
import psutil


def system_name():
    """
    Name of the operating system.

    Returns "Darwin" on macOS, "Windows" or "Linux".

    Returns:
        str: name of the platform
    """
    return platform.system()


def user_name():
    return getpass.getuser()


def home_path():
    return os.path.expanduser('~')


def executable_path():
    pass


def application_path():
    pass


def application_user_path():
    pass


def ram_size():
    """
    Total physical memory available.

    Returns:
        int: memory size in bytes
    """
    return psutil.virtual_memory().total
