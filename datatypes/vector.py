import numpy as np


class V2i:
    def __init__(self, x=0, y=0):
        self.data = np.array([x, y], dtype=np.int32)

    @property
    def x(self):
        return self.data[0]

    @x.setter
    def x(self, value):
        self.data[0] = value

    @property
    def y(self):
        return self.data[1]

    @y.setter
    def y(self, value):
        self.data[1] = value

    def __add__(self, other):
        return V2i(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return V2i(self.x - other.x, self.y - other.y)

    def __eq__(self, other):
        return np.array_equal(self.data, other.data)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f"V2i({self.x}, {self.y})"

    def __str__(self):
        return f"({self.x}, {self.y})"


class V2f:
    def __init__(self, x, y):
        self.data = np.array([x, y], dtype=np.float32)

    @property
    def x(self):
        return self.data[0]

    @property
    def y(self):
        return self.data[1]

    @x.setter
    def x(self, value):
        self.data[0] = value

    @y.setter
    def y(self, value):
        self.data[1] = value

    def __add__(self, other):
        return V2f(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return V2f(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        return V2f(self.x * other, self.y * other)

    def __truediv__(self, other):
        return V2f(self.x / other, self.y / other)

    def __eq__(self, other):
        return np.array_equal(self.data, other.data)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f"V2f({self.x}, {self.y})"

    def __str__(self):
        return f"({self.x}, {self.y})"
