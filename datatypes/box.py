import numpy as np

from datatypes.vector import V2i, V2f


class Box2i:
    def __init__(self, min=0, max=0):
        self.data = np.zeros((2, 2), dtype=np.int32)
        self.min = min
        self.max = max

    @property
    def min(self):
        return V2i(*self.data[0])

    @min.setter
    def min(self, value):
        self.data[0] = value

    @property
    def max(self):
        return V2i(*self.data[1])

    @max.setter
    def max(self, value):
        self.data[1] = value

    def size(self):
        return self.max - self.min + V2i(1, 1)

    def __eq__(self, other):
        return np.array_equal(self.data, other.data)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f"Box2i({self.min}, {self.max})"

    def __str__(self):
        return f"[{self.min}, {self.max}]"


class Box2f:
    def __init__(self, min=0.0, max=0.0):
        self.data = np.zeros((2, 2), dtype=np.float16)
        self.min = min
        self.max = max

    @property
    def min(self):
        return V2f(*self.data[0])

    @min.setter
    def min(self, value):
        self.data[0] = value

    @property
    def max(self):
        return V2f(*self.data[1])

    @max.setter
    def max(self, value):
        self.data[1] = value

    def size(self):
        return self.max - self.min + V2f(1, 1)

    def __eq__(self, other):
        return np.array_equal(self.data, other.data)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return f"Box2i({self.min}, {self.max})"

    def __str__(self):
        return f"[{self.min}, {self.max}]"
