


class Buffer:
    _bounds = None
    _pixels = None

    def __init__(self, width: int = None, height: int = None, channels: int = None):
        pass

    def width(self):
        pass

    def height(self):
        pass

    def channels(self):
        pass

    def empty(self):
        return self._pixels is None

    def unique(self):
        return self._pixels.unique()

    def clear(self):
        pass

    def bounds(self):
        return self._bounds

