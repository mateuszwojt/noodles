import logging

from OpenGL.GL import *


def check_gl_errors():
    """Python equivalent of C++ check_gl_errors()"""
    err = glGetError()
    if err != GL_NO_ERROR:
        logging.error(f"OpenGL error: {err}")


def clear_gl_errors():
    """Clears all pending OpenGL errors."""
    while True:
        error = glGetError()
        if error == GL_NO_ERROR:
            break
