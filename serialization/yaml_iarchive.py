import logging
import os.path

import yaml

from serialization.config import MAGIC_TOKEN_KEY, MAGIC_TOKEN_VALUE, VERSION_TOKEN_KEY, VERSION_TOKEN_VALUE


class YamlIArchive:
    """This class reads content of a YAML composition file"""
    _data = None
    _header_read = False

    def __init__(self, path):
        if not os.path.exists(path):
            logging.error('Cannot open %s. File does not exist.' % path)
            return

        with open(path, 'r') as stream:
            try:
                self._data = yaml.load(stream, Loader=yaml.FullLoader)
            except yaml.YAMLError as e:
                logging.error('Error while reading YAML archive: %s' % e)

            print(self._data)

    def read_composition_header(self):
        if not self._data:
            logging.debug('File stream is empty.')
            return False

        # read magic values from the header
        if self._data.get(MAGIC_TOKEN_KEY) != MAGIC_TOKEN_VALUE:
            logging.debug('Header does not contain valid magic token.')
            return False

        if self._data.get(VERSION_TOKEN_KEY) != VERSION_TOKEN_VALUE:
            logging.debug('Header version token does not match.')
            return False

        self._header_read = True
        return self._header_read

    def get_value(self, key):
        return self._data.get(key)
