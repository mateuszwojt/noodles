import io

import yaml

from serialization.config import MAGIC_TOKEN_KEY, MAGIC_TOKEN_VALUE, VERSION_TOKEN_KEY, VERSION_TOKEN_VALUE


class YamlOArchive:
    _data = {}
    _out_path = None
    _header_written = False

    def __init__(self, data):
        self._data = data

    def write_composition_header(self):
        # add header tokens to the data
        self._data = {
            **self._data,
            MAGIC_TOKEN_KEY: MAGIC_TOKEN_VALUE,
            VERSION_TOKEN_KEY: VERSION_TOKEN_VALUE,
        }
        self._header_written = True
        return self._header_written

    def write_to_file(self, path):
        with io.open(path, 'w', encoding='utf8') as outfile:
            yaml.dump(self._data, outfile, default_flow_style=False, allow_unicode=True)

    def header_written(self):
        return self._header_written
